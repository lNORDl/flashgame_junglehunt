package com.antkarlov.animation
{
	import flash.utils.setTimeout;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * AntAnimCache класс хранилище анимаций (синглтон).
	 * 
	 * @langversion ActionScript 3
	 * @playerversion Flash 9.0.0
	 * 
	 * @author Anton Karlov (http://www.ant-karlov.ru)
	 * @since  01.04.2012
	 */
	public class AntAnimCache extends Object
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		/**
		 * Указатель на метод который будет вызываться по мере обработки клипов.
		 * В качестве атрибута в метод передается текущий процент от выполненной работы:
		 * function onProgress(percent:Number):void { ... }
		 */
		public var onProgressCallback:Function = null;
		
		/**
		 * Указатель на метод который будет вызыван когда процесс кэширования будет завершен.
		 */
		public var onCompleteCallback:Function = null;
		
		/**
		 * Сколько клипов обработать за один шаг. Чем больше клипов обрабатывается за один шаг
		 * тем быстрее выполняется кэширование. Но при большом количестве обрабатываемых клипов
		 * есть вероятность подвесить плеер и получить сообщение о зависшем плеере. Поэтому кол-во
		 * клипов обрабатываемых за один шаг следует подбирать исходя из размера и количества кадров
		 * на клип.
		 */
		public var clipsPerStep:int = 10;
		
		/**
		 * Если true то при создании новых анимаций с повторяющимися ключами старые анимации будут 
		 * заменены новыми.
		 */
		public var replaceExisting:Boolean = true;
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		protected static var _instance:AntAnimCache;
		
		protected var _cacheQueue:Array; // Очердь клипов для обработки.
		protected var _curProcessingItem:int; // Индекс текущего/последнего обработанного клипа.
		protected var _animations:Object; // Коллекция анимаций.
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
		
		/**
		 * @constructor
		 */
		public function AntAnimCache()
		{
			super();
			
			if (_instance != null)
			{
				throw new Error("AntAnimCache is a singleton class. Please use getInstace() to get reference.");
				return;
			}
			
			_instance = this;
			_cacheQueue = [];
			_curProcessingItem = 0;
			_animations = {};
		}
		
		/**
		 * @private
		 */
		public static function getInstance():AntAnimCache
		{
			return _instance == null ? new AntAnimCache() : _instance;
		}
		
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		/**
		 * Выполняет кэширование клипа и возвращает анимацию.
		 * 
		 * @param	key	 Имя класса клипа который необходимо растеризировать.
		 * В последствии имя класса клипа будет использоваться как его уникальный идентификатор в кэше.
		 * 
		 * @return		Воозвращает растровую анимацию клипа AntAnim.
		 */
		public function cacheAnim(clipClass:Class):AntAnim
		{
			var anim:AntAnim = null;
			var key:String = getQualifiedClassName(clipClass);
			
			if (!_animations[key] || replaceExisting)
			{
				anim = new AntAnim();
				anim.cacheFromLib(clipClass);
				_animations[key] = anim;
			}
			else
			{
				anim = _animations[key];
			}
			
			return anim;
		}
		
		/**
		 * Извлекает копию анимации из кэша по идентификатору.
		 * 
		 * @param	key	 Имя класса клипа анимацию которого необходимо извлеч.
		 * @return		Возвращает копию анимации из кэша (копируется только экземпляр анимации, кадры общие).
		 */
		public function getAnim(key:String):AntAnim
		{
			if (!_animations[key])
			{
				throw new Error("AntAnimCache::getAnim() - ERROR: Missing animation \"" + key + "\".");
				return null;
			}
			
			return _animations[key].clone();
		}
		
		/**
		 * Добавляет имя клипа в очередь для кэширования.
		 * 
		 * @param	clipName	 Имя класса клипа которых необходимо растеризировать.
		 */
		public function addClipToCacheQueue(clipClass:Class):void
		{
			_cacheQueue[_cacheQueue.length] = clipClass;
		}
		
		/**
		 * Добавляет несколько имен клипов в очередь для кэширования.
		 * 
		 * @param	clipNames	 Массив имен классов клипов которые необходимо растеризировать.
		 */
		public function addClipsToCacheQueue(clipClasses:Array /* of Classes */):void
		{
			var n:int = clipClasses.length;
			for (var i:int = 0; i < n; i++)
			{
				_cacheQueue[_cacheQueue.length] = clipClasses[i];
			}
		}
		
		/**
		 * Запускает процесс кэширования.
		 */
		public function makeCache():void
		{
			clipsPerStep = (clipsPerStep == 1) ? 2 : clipsPerStep;
			_curProcessingItem = 0;
			step();
		}
		
		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * Шаг процесса кэширования.
		 */
		protected function step():void
		{
			// Обрабатываем заданное количество клипов за один шаг.
			var n:int = _curProcessingItem + clipsPerStep;
			n = (n >= _cacheQueue.length) ? _cacheQueue.length : n;
			for (var i:int = _curProcessingItem; i < n; i++)
			{
				cacheAnim(_cacheQueue[i]);
			}
			
			_curProcessingItem = n - 1;
			
			// Отправляем сообщение о прогрессе.
			if (onProgressCallback != null)
			{
				(onProgressCallback as Function).apply(this, [ (_curProcessingItem + 1) / _cacheQueue.length ]);
			}
			
			// Если процесс завершен, отправляем сообщение о завершении.
			if (_curProcessingItem == _cacheQueue.length - 1)
			{
				_cacheQueue.length = 0;
				if (onCompleteCallback != null)
				{
					(onCompleteCallback as Function).apply(this);
				}
			}
			else
			{
				setTimeout(step, 1);
			}
		}

	}

}