package com.game.interfaces
{
	
	public interface IGameObject
	{
		
		function update(delta:Number):void;
		
		function destroy():void;
		
		function init(posX:Number, posY:Number, rot:Number):void;
		
	}
	
}