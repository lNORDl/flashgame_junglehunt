package com.game
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.Joints.b2DistanceJoint;
	import Box2D.Dynamics.Joints.b2DistanceJointDef;
	import Box2D.Dynamics.Joints.b2Joint;
	import Box2D.Dynamics.Joints.b2JointDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.controllers.ObjectController;
	import com.game.levels.BasicLevel;
	import com.game.managers.LevelManager;
	import com.game.managers.ScreenManager;
	import com.game.objects.arrows.WoodDart;
	import com.game.objects.BasicObject;
	import com.game.objects.bombs.BlackBomb;
	import com.game.objects.Hero;
	import com.game.objects.Kind;
	import com.game.screens.ScreenMainMenu;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	public class Universe extends Sprite
	{
		
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		public static const RATIO:uint = 30;
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------	
		
		// Переменные для физики
		public var iterations:int;
		public var timeStep:Number;
		public var drawScale:Number;
		
		public var physics:b2World;
		public var testPhysics:b2World;
		
		public var canContact:Boolean = false;
			
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		private static var _instance:Universe;
		
		private var _levelManager:LevelManager = LevelManager.getInstance();
		
		private var _deltaTime:Number = 0; // Текущее delta время
		private var _lastTick:int = 0; // Последний тик таймера (для расчета нового delta времени)
		private var _maxDeltaTime:Number = 0.03;
		
		private var Objects:ObjectController;
		private var Cache:Array = [];
		
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
		public function Universe()
		{
			_instance = this;
		}
		
		public function init():void
		{							
			Objects = new ObjectController();
			
			initPhysics();
		}
		
		// Основной игровой "Цикл"
		public function enterFrame():void
		{	
			if (_levelManager.currentLevel !== null)
			{						
				//Процессинг физики
				physics.Step(timeStep, iterations);	
				
				// Процессинг игровых объектов
				Objects.update(1);
			}
		}		
		
		// Добавление игрового объекта
		public function addObject(object:BasicObject):void
		{
			Objects.add(object);
		}
		
		// Удаление игрового объекта
		public function removeObject(object:BasicObject):void
		{
			Objects.remove(object);
		}
		
		public function addToCache(object:BasicObject):void
		{
			Cache.push(object);
		}
		
		public function getFromCache(tag:uint):BasicObject
		{
			for (var i:int = 0; i < Cache.length; i++)
			{
				var object:BasicObject = Cache[i];
				if (object.myTag == tag)
				{
					Cache[i] = null;
					Cache.splice(i, 1);
					return object;
				}
			}
			
			return null;
		}
		
		public function removeFromCache(object:BasicObject):void
		{
			for (var i:int = 0; i < Cache.length; i++)
			{
				var _object:BasicObject = Cache[i];
				
				if (_object == object)
				{
					Cache[i] = null;
					Cache.splice(i, 1);
					break;
				}
			}			
		}
		
		public function clearPhysics():void
		{			
			for (var jj:b2Joint = physics.GetJointList(); jj; jj = jj.GetNext())
			{ 
				physics.DestroyJoint(jj);
			}	
			
			for (var bb:b2Body = physics.GetBodyList(); bb; bb = bb.GetNext())
			{				
				physics.DestroyBody(bb);
			}
			
			physics.Step(timeStep, iterations);	
		}
		
		// Инициализация физики		
		private function initPhysics():void
		{
			iterations = 5;
			timeStep = 1 / 35;
			drawScale = 30;
			
			var worldAABB:b2AABB = new b2AABB();
			worldAABB.lowerBound.Set(-50, -50);
			worldAABB.upperBound.Set(50, 50);
			physics = new b2World(worldAABB, new b2Vec2(0, 9.8), true);
			testPhysics = new b2World(worldAABB, new b2Vec2(0, 9.8), true);
			
			physics.SetContactListener(new ContactListener());
			
			// Инициализация отладочной отрисовки
			//var _debugLayer:Sprite = new Sprite();
			//addChild(_debugLayer);
			//var debugDraw:b2DebugDraw = new b2DebugDraw();
			//debugDraw.m_sprite = _debugLayer;
			//debugDraw.m_drawScale = drawScale;
			//debugDraw.m_fillAlpha = 0.1;
			//debugDraw.m_lineThickness = 1;
			//debugDraw.m_drawFlags = b2DebugDraw.e_shapeBit|b2DebugDraw.e_jointBit;
			//physics.SetDebugDraw(debugDraw);
		}
		
		//---------------------------------------
		// GETTER / SETTERS
		//---------------------------------------
		public static function getInstance():Universe
		{
			return (_instance == null) ? new Universe() : _instance;
		}
		
		public function getObjects():ObjectController
		{
			return Objects;
		}
	}

}