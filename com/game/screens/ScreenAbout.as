package com.game.screens 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.buttons.ButtonMenu;
	import com.game.objects.Kind;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenAbout extends BasicScreen
	{
		
		public function ScreenAbout() 
		{
			
		}
		
		override public function init():void
		{
			myTag = Kind.SCREEN_ABOUT;
			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ScreenAbout_mc", null, true);
			
			_myPosition = new Point(300, 225);
			
			_myContainer = _game.screenContainer;
			
			
			super.init();
		}
		
		public static function create():BasicScreen
		{
			var _screen:BasicScreen = new ScreenAbout();
			
			
			return _screen;
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		override public function createButtons():void
		{
			var _buttonMenu:BasicButton = ButtonMenu.create(this, new Point(0, 170));
		}
		
		override public function fadeOut():void
		{			
			super.fadeOut();
		}
		override public function fadeIn():void
		{			
			super.fadeIn();
		}
		
	}

}