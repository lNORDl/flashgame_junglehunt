package com.game.screens 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.buttons.ButtonAbout;
	import com.game.buttons.ButtonMore;
	import com.game.buttons.ButtonPlay;
	import com.game.buttons.ButtonSound;
	import com.game.objects.Kind;
	import com.greensock.TweenLite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class ScreenMainMenu extends BasicScreen
	{
		private var _buttonPlay:BasicButton;
		private var _buttonAbout:BasicButton;
		private var _buttonMore:BasicButton;
		private var _buttonSound:BasicButton;
		
		public function ScreenMainMenu() 
		{
			
		}
		
		override public function init():void
		{
			myTag = Kind.SCREEN_MAINMENU;
			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ScreenMainMenu_mc", null, true);
			
			_myPosition = new Point(300, 225);
			
			_myContainer = _game.screenContainer;
			
			_elementsCount = 4;
			
			
			super.init();
		}
		
		public static function create():BasicScreen
		{
			var _screen:BasicScreen = new ScreenMainMenu();
			
			
			return _screen;
		}
		
		override public function destroy():void
		{
			_buttonAbout = null;
			_buttonMore = null;
			_buttonPlay = null;
			_buttonSound = null;
			
			
			super.destroy();
		}
		
		override public function createButtons():void
		{
			_buttonPlay = ButtonPlay.create(this, new Point(0, 300));
			_buttonMore = ButtonMore.create(this, new Point(0, 300));
			_buttonAbout = ButtonAbout.create(this, new Point(0, 300));
			_buttonSound = ButtonSound.create(this, new Point(250, -176));
			_buttonSound.alpha = 0;
		}
		
		override public function fadeOut():void
		{			
			super.fadeOut();
			
			
			TweenLite.to(_buttonPlay, 0.7, {x:0, y:300, onComplete:addReady});
			TweenLite.to(_buttonAbout, 0.7, {x:0, y:300, onComplete:addReady});
			TweenLite.to(_buttonMore, 0.7, {x:0, y:300, onComplete:addReady});
			TweenLite.to(_buttonSound, 0.7, {alpha:0, onComplete:addReady});
		}
		override public function fadeIn():void
		{			
			super.fadeIn();
			
			
			TweenLite.to(_buttonPlay, 1, {x:0, y:-70, onComplete:addReady});
			TweenLite.to(_buttonAbout, 1, {x:0, y:20, onComplete:addReady});
			TweenLite.to(_buttonMore, 1, {x:0, y:110, onComplete:addReady});
			TweenLite.to(_buttonSound, 1, {alpha:1, onComplete:addReady});
		}
		
	}

}