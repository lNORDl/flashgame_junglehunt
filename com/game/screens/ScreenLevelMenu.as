package com.game.screens 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.buttons.ButtonSound;
	import com.game.buttons.ButtonStartLevel;
	import com.game.managers.LevelManager;
	import com.game.objects.Kind;
	import com.greensock.TweenLite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenLevelMenu extends BasicScreen
	{
		
		private var _levelsCount:int = 0;
		private var _buttonSound:BasicButton;
		
		public function ScreenLevelMenu() 
		{
			
		}
		
		
		override public function init():void
		{
			myTag = Kind.SCREEN_LEVELMENU;
			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ScreenLevelMenu_mc", null, true);
			
			_myPosition = new Point(300, 225);
			
			_myContainer = _game.screenContainer;
			
			_levelsCount = LevelManager.MAX_LEVEL;
			
			
			super.init();
		}
		
		public static function create():BasicScreen
		{
			var _screen:BasicScreen = new ScreenLevelMenu();
			
			
			return _screen;
		}
		
		override public function destroy():void
		{
			_buttonSound = null;
			
			super.destroy();
		}
		
		override public function createButtons():void
		{
			//_buttonSound = ButtonSound.create(this, new Point(250, -176));
			//_buttonSound.alpha = 0;			
			
			var _posX:int = -215;
			var _posY:int = -146;
			
			for (var i:int = 1; i < LevelManager.MAX_LEVEL + 1; i++)
			{
				var _button:BasicButton = ButtonStartLevel.create(this, new Point(_posX, _posY), i);
				_posX += 85;
				if ((i / 6 == 1) || (i / 6 == 2) || (i / 6 == 3) || (i / 6 == 4) || (i / 6 == 5))
				{
					_posX = -215;
					_posY += 70;
				}
			}
		}
		
		override public function fadeOut():void
		{			
			super.fadeOut();
			
			//TweenLite.to(_buttonSound, 0.7, {alpha:0, onComplete:addReady});
		}
		override public function fadeIn():void
		{			
			super.fadeIn();
			
			//TweenLite.to(_buttonSound, 1, {alpha:1, onComplete:addReady});
		}
		
	}

}