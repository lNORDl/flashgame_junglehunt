package com.game.screens 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.Game;
	import com.game.managers.LevelManager;
	import com.game.managers.ScreenManager;
	import com.game.objects.Kind;
	import com.game.Universe;
	import com.game.windows.BasicWindow;
	import com.greensock.TweenLite;
	import fl.motion.ITween;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicScreen extends Sprite
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var canUse:Boolean = false;
		public var canFadeOut:Boolean = false;
		public var canDestroy:Boolean = false;
		
		public var myTag:int;
		
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		protected var _game:Game = Game.getInstance();
		protected var _screenManager:ScreenManager = ScreenManager.getInstance();
		protected var _levelManager:LevelManager = LevelManager.getInstance();
		protected var _myContainer:Sprite;
		
		protected var _myActor:AntActor;
		protected var _myPosition:Point;
		
		protected var _elementsCount:int = 0;
		protected var _elementsReady:int = 0;
		
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		private var _isFadeIn:Boolean = false;
		private var _isFadeOut:Boolean = false;
		
		private var _buttons:Array = [];
		private var _windows:Array = [];
		
		
		public function BasicScreen() 
		{
			
		}
		
		public function init():void
		{			
			if (_myActor !== null)
			{
				addChild(_myActor);
			}
			
			if (_myPosition !== null)
			{
				this.x = _myPosition.x;
				this.y = _myPosition.y;
			}
			
			_myContainer.addChild(this);
			
			createButtons();
			
			_elementsCount ++;
			
			fadeIn();
		}
		
		public function update():void
		{
			if (_isFadeIn && _elementsReady == _elementsCount)
			{
				onFadeInComplete();
			}
			
			if (_isFadeOut && _elementsReady == _elementsCount)
			{
				onFadeOutComplete();
			}
			
			for (var i:int = 0; i < _windows.length; i++)
			{
				_windows[i].update();
			}
		}
		
		public function destroy():void
		{			
			for (var i:int = 0; i < _buttons.length; i++)
			{
				(_buttons[i] as BasicButton).destroy();
			}
			
			for (var j:int = 0; j < _windows.length; j++)
			{
				(_windows[j] as BasicWindow).destroy();
			}
			
			if (_myActor !== null)
			{
				_myActor.free();
				_myActor = null;
			}
			_myContainer.removeChild(this);
			_myContainer = null;
			_game = null;
			_screenManager = null;
			_levelManager = null;
			_buttons = null;
			_windows = null;
		}
		
		public function createButtons():void
		{
			
		}
		
		public function fadeIn():void
		{
			_isFadeIn = true;
			
			this.alpha = 0;
			TweenLite.to(this, 0.5, {alpha:1, onComplete:addReady});
		}
		public function fadeOut():void
		{			
			_isFadeOut = true;
			canFadeOut = false;
			canUse = false;
			
			for (var j:int = 0; j < _windows.length; j++)
			{
				(_windows[j] as BasicWindow).fadeOut();
			}
			
			TweenLite.to(this, 0.5, {alpha:0, onComplete:addReady});
		}
		
		protected function onFadeInComplete():void
		{
			_isFadeIn = false;
			canUse = true;
			canFadeOut = true;
			_elementsReady = 0;
		}
		protected function onFadeOutComplete():void
		{
			_isFadeOut = false;
			canDestroy = true;
		}	
		
		protected function addReady():void
		{
			_elementsReady ++;
		}
		
		public function addButton(button:BasicButton):void
		{
			_buttons.push(button);	
		}
		
		public function addWindow(window:BasicWindow):void
		{
			_windows.push(window);
		}
		
		public function removeWindow(window:BasicWindow):void
		{
			if (_windows.length !== 0)
			{
				for (var i:int = 0; i < _windows.length; i++)
				{
					if (_windows[i] == window)
					{
						_windows[i].destroy();
						_windows[i] = null;
						_windows.splice(i, 1);
						break;
					}
				}
			}
		}
	}

}