package com.game.screens 
{
	import com.antkarlov.animation.AntActor;
	import com.game.levels.BasicLevel;
	import com.game.objects.Kind;
	import com.game.windows.WindowGameInterface;
	import com.game.windows.WindowStartLevel;
	import com.greensock.TweenLite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenGameLevel extends BasicScreen
	{
		
		public var numLevel:int = 0;
		
		private var _myLevel:BasicLevel;
		
		public function ScreenGameLevel() 
		{
			
		}
		
		override public function init():void
		{
			myTag = Kind.SCREEN_GAMELEVEL;
			
			_myPosition = new Point(300, 225);
			
			_myContainer = _game.screenContainer;
			
			
			super.init();
		}
		
		public static function create():BasicScreen
		{
			var _screen:BasicScreen = new ScreenGameLevel();
			
			
			return _screen;
		}
		
		override public function update():void
		{					
			super.update();
		}
		
		override public function destroy():void
		{
			_levelManager.destroyLevel();
			
			
			super.destroy();
		}
		
		override public function createButtons():void
		{
			var _windowGameInterface:WindowGameInterface = WindowGameInterface.create(this) as WindowGameInterface;
			
			_myLevel = _levelManager.createLevel(numLevel);	
			_myLevel.init(this, _windowGameInterface);
			
			var _windowStartLevel:WindowStartLevel = WindowStartLevel.create(this) as WindowStartLevel;
		}
		
		override public function fadeOut():void
		{			
			TweenLite.to(_game.gameContainer, 0.5, { alpha:0 } );
			TweenLite.to(_game.ogContainer, 0.5, { alpha:0 } );
			_myLevel.canPlay = false;
			
			
			super.fadeOut();			
		}
		override public function fadeIn():void
		{			
			TweenLite.to(_game.gameContainer, 0.5, { alpha:1 } );
			TweenLite.to(_game.ogContainer, 0.5, { alpha:1 } );
			
			
			super.fadeIn();
		}		
	}

}