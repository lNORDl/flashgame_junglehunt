package com.game
{
	
	import Box2D.Dynamics.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Dynamics.Contacts.b2ContactResult;
	import Box2D.Dynamics.Joints.b2DistanceJoint;
	import Box2D.Dynamics.Joints.b2Joint;
	import Box2D.Dynamics.Joints.b2JointEdge;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.game.objects.arrows.BasicArrow;
	import com.game.objects.BasicObject;
	import com.game.objects.bombs.BasicBomb;
	import com.game.objects.Kind;
	import com.game.objects.ropes.BasicRope;
	import com.game.objects.walls.BasicWall;
	
	public class ContactListener extends b2ContactListener
	{
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		private var _universe:Universe;
		
		private var object1:BasicObject = null;
		private var object2:BasicObject = null;
		private var object1Body:b2Body = null;
		private var object2Body:b2Body = null;
		private var object1Shape:b2Shape = null;
		private var object2Shape:b2Shape = null;
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
		
		/**
		 * @constructor
		 */
		public function ContactListener()
		{
			_universe = Universe.getInstance()
		}
		
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override public function Add(point:b2ContactPoint):void
		{
			if (_universe.canContact)
			{
				
			object1Shape = point.shape1;
			object2Shape = point.shape2;
			object1Body = object1Shape.GetBody();
			object2Body = object2Shape.GetBody();
			object1 = object1Body.GetUserData();
			object2 = object2Body.GetUserData();
			
			//trace("Фигура1: " + object1Shape + "Тело1: " + object1Body + "Объект1: " + object1);
			//trace("Фигура2: " + object2Shape + "Тело2: " + object2Body + "Объект2: " + object2);
			
			var _obj1IsVisible:Boolean = false;
			var _obj2IsVisible:Boolean = false;
			
			if (object1.mySprite !== null)
			{
				if (object1.contains(object1.mySprite))
				{
					_obj1IsVisible = true;
				}				
			}
			else if (object1.myActor !== null)
			{
				if (object1.contains(object1.myActor))
				{
					_obj1IsVisible = true;
				}					
			}	
			
			if (object2.mySprite !== null)
			{
				if (object2.contains(object2.mySprite))
				{
					_obj2IsVisible = true;
				}				
			}
			else if (object2.myActor !== null)
			{
				if (object2.contains(object2.myActor))
				{
					_obj2IsVisible = true;
				}					
			}
			
			if (object1.myGroup !== Kind.GROUP_NONE && object1.myTag !== Kind.TAG_NONE && 
			object2.myGroup !== Kind.GROUP_NONE && object2.myTag !== Kind.TAG_NONE && 
			object1.myGroup !== Kind.GROUP_EFFECT && object2.myGroup !== Kind.GROUP_EFFECT &&
			object1.isExists == true && object2.isExists == true && _obj1IsVisible && _obj2IsVisible)
			{			
				
			// Деревянная стрела
			if (object2Shape.GetUserData() == "TipWoodDart")
			{
				if ((object1.myGroup == Kind.GROUP_WOOD_WALL) || (object1.myGroup == Kind.GROUP_WOOD_PLANK))
				{		
					(object2 as BasicArrow).stickBody = object1Body;
					(object2 as BasicArrow).type = "STICK";
					
					if (object1.myGroup !== Kind.GROUP_WOOD_PLANK)
					{
						object2Body.SetAngularVelocity(0);
						object2Body.SetLinearVelocity(new b2Vec2(0, 0));
					}				
				}
				
				if (object1.myGroup == Kind.GROUP_THREAD_ROPE)
				{
					object1.type = "CUT";
				}
				
				if (object1.myGroup == Kind.GROUP_PITCHER && !(object2 as BasicArrow).isFirsFrame)
				{
					object1.type = "HITP";
				}
			}				
			// Инверсия
			if (object1Shape.GetUserData() == "TipWoodDart")
			{
				if ((object2.myGroup == Kind.GROUP_WOOD_WALL) || (object2.myGroup == Kind.GROUP_WOOD_PLANK))
				{				
					(object1 as BasicArrow).stickBody = object2Body;
					(object1 as BasicArrow).type = "STICK";
					
					if (object2.myGroup !== Kind.GROUP_WOOD_PLANK)
					{
						object1Body.SetAngularVelocity(0);
						object1Body.SetLinearVelocity(new b2Vec2(0, 0));
					}
				}
				
				if (object2.myGroup == Kind.GROUP_THREAD_ROPE)
				{
					object2.type = "CUT";
				}				
				
				if (object2.myGroup == Kind.GROUP_PITCHER && !(object1 as BasicArrow).isFirsFrame)
				{
					object2.type = "HITP";
				}
			}	
			
			// Железная стрела
			if (object2Shape.GetUserData() == "TipIronDart")
			{
				if ((object1.myGroup == Kind.GROUP_WOOD_WALL) || (object1.myGroup == Kind.GROUP_WOOD_PLANK) || (object1.myGroup == Kind.GROUP_IRON_WALL) || (object1.myGroup == Kind.GROUP_IRON_PLANK))
				{									
					(object2 as BasicArrow).stickBody = object1Body;
					(object2 as BasicArrow).type = "STICK";
					
					if (object1.myGroup !== Kind.GROUP_WOOD_PLANK)
					{
						object2Body.SetAngularVelocity(0);
						object2Body.SetLinearVelocity(new b2Vec2(0, 0));
					}
				}
				
				if ((object1.myGroup == Kind.GROUP_THREAD_ROPE) || (object1.myGroup ==  Kind.GROUP_IRON_ROPE))
				{
					object1.type = "CUT";
				}
				
				if (object1.myGroup == Kind.GROUP_PITCHER && !(object2 as BasicArrow).isFirsFrame)
				{
					object1.type = "HITP";
				}
			}				
			// Инверсия
			if (object1Shape.GetUserData() == "TipIronDart")
			{
				if ((object2.myGroup == Kind.GROUP_WOOD_WALL) || (object2.myGroup == Kind.GROUP_WOOD_PLANK) || (object2.myGroup == Kind.GROUP_IRON_WALL) || (object2.myGroup == Kind.GROUP_IRON_PLANK))
				{									
					(object1 as BasicArrow).stickBody = object2Body;
					(object1 as BasicArrow).type = "STICK";
					
					if (object2.myGroup !== Kind.GROUP_WOOD_PLANK)
					{
						object1Body.SetAngularVelocity(0);
						object1Body.SetLinearVelocity(new b2Vec2(0, 0));
					}
				}
				
				if ((object2.myGroup == Kind.GROUP_THREAD_ROPE) || (object2.myGroup ==  Kind.GROUP_IRON_ROPE))
				{
					object2.type = "CUT";
				}
				
				if (object2.myGroup == Kind.GROUP_PITCHER && !(object1 as BasicArrow).isFirsFrame)
				{
					object2.type = "HITP";
				}
			}	
			
			// ВзрывающаясяСтрела
			if (object2.myTag == Kind.TAG_BOMB_DART)
			{
				if (object1.myGroup == Kind.GROUP_WOOD_PLANK && object1.myTag !== Kind.TAG_BLACK_BOMB_BOX && object1.myTag !== Kind.TAG_WOOD_BOX1)
				{									
					(object1 as BasicWall).type = "DESTROY";
				}
				
				if (object1.myGroup !== Kind.GROUP_TRIGGER && object1.myGroup !== Kind.GROUP_DART)
				{
					(object2 as BasicArrow).type = "HIT";	
				}
				//trace(object1, object1.myTag, object1.myGroup, object1.myBody, object1.mySprite);
			}	
			// Инверсия
			if (object1.myTag == Kind.TAG_BOMB_DART)
			{
				if (object2.myGroup == Kind.GROUP_WOOD_PLANK && object2.myTag !== Kind.TAG_BLACK_BOMB_BOX  && object2.myTag !== Kind.TAG_WOOD_BOX1)
				{									
					(object2 as BasicWall).type = "DESTROY";
				}
				
				if (object2.myGroup !== Kind.GROUP_TRIGGER && object2.myGroup !== Kind.GROUP_DART)
				{
					(object1 as BasicArrow).type = "HIT";
				}
				//trace(object2, object2.myTag, object2.myGroup, object2.myBody, object2.mySprite);
			}	
			
			// Бомба
			if (object2.myTag == Kind.TAG_BLACK_BOMB)
			{
				if (object1Shape.GetUserData() == "HitBlckBobmSensor")
				{
					(object2 as BasicBomb).myBox = object1Body;
					object2.type = "COMPLETE";
				}
			}
			
			// Триггеры
			if (object2.myGroup == Kind.GROUP_TRIGGER)
			{
				if (object1.myGroup == Kind.GROUP_DART || object1.myGroup == Kind.GROUP_BLADE)
				{
					object2.type = "HIT";
				}
			}			
			// Инверсия
			if (object1.myGroup == Kind.GROUP_TRIGGER)
			{
				if (object2.myGroup == Kind.GROUP_DART || object2.myGroup == Kind.GROUP_BLADE)
				{
					object1.type = "HIT";
				}
			}
			
			// Кувшины
			if (object2.myGroup == Kind.GROUP_PITCHER)
			{
				if (object1.myGroup == Kind.GROUP_WOOD_WALL || object1.myGroup == Kind.GROUP_BLADE
				|| object1.myGroup == Kind.GROUP_WOOD_PLANK || object1.myGroup == Kind.GROUP_IRON_WALL)
				{
					object2.type = "HITP";					
				}
				
				//if (object1.myGroup == Kind.GROUP_DART)
				//{
					//if (!(object1 as BasicArrow).isFirsFrame)
					//{
						//object2.type = "HITP";
						//trace(object1, object1.myBody, object1.mySprite);
					//}
				//}
			}	
			// Инверсия
			if (object1.myGroup == Kind.GROUP_PITCHER)
			{
				if (object2.myGroup == Kind.GROUP_WOOD_WALL || object2.myGroup == Kind.GROUP_BLADE
				|| object2.myGroup == Kind.GROUP_WOOD_PLANK || object2.myGroup == Kind.GROUP_IRON_WALL)
				{
					object1.type = "HITP";					
				}
				
				//
				//if (object2.myGroup == Kind.GROUP_DART)
				//{
					//if (!(object2 as BasicArrow).isFirsFrame)
					//{
						//object1.type = "HITP";
						//trace(object2, object2.myBody, object2.mySprite);
					//}
				//}
			}
			
			// Деревянное лезвие
			if (object2.myTag == Kind.TAG_WOOD_BLADE)
			{
				if (object1.myGroup == Kind.GROUP_THREAD_ROPE)
				{
					object1.type = "CUT";
				}
			}		
			
			// Железное лезвие
			if (object2.myTag == Kind.TAG_IRON_BLADE)
			{
				if (object1.myGroup == Kind.GROUP_THREAD_ROPE || object1.myGroup == Kind.GROUP_IRON_ROPE)
				{
					object1.type = "CUT";
				}
			}
			
			}
			}
		}	
		
		/**
		 * @inheritDoc
		 */
		override public function Persist(point:b2ContactPoint):void
		{
			
		}
		
		/**
		 * @inheritDoc
		 */
		override public function Remove(point:b2ContactPoint):void
		{
			//
		}
		
		/**
		 * @inheritDoc
		 */
		override public function Result(point:b2ContactResult):void
		{
			//
		}
		
	}

}