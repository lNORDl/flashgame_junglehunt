package com.game.managers 
{
	import com.game.objects.Kind;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class SoundManager extends Object
	{
		private static var _instance:SoundManager;
		
		public var isPlay:Boolean = false;
		
		private var _soundChannel:SoundChannel = new SoundChannel();
		
		private var _mainSoundChanel:SoundChannel = new SoundChannel();
		private var _mainTransform:SoundTransform = new SoundTransform();
		private var _mainSound:Sound = new SoundJungleHunt();
		private var _mainSoundLastPosition:int;
		
		
		public function SoundManager() 
		{
			_instance = this;
		}
		
		public function init():void
		{
			isPlay = true;
			
			_mainTransform.volume = 0.2;
			playMainSound(0);
		}
		
		private function soundCompleteHandler(e:Event):void 
		{
			playMainSound(0);
		}
		
		public function playSound(tag:int):void
		{
			if (isPlay)
			{
				var _sound:Sound = null;
				var _soundTransform:SoundTransform = new SoundTransform();
				
				switch(tag)
				{
					case Kind.SOUND_BUTTONCLICK:
					_sound = new ButtonCLick();
					break;
					
					case Kind.SOUND_LEVELCOMPLETE:
					_sound = new SoundLevelComplete();
					_soundTransform.volume = 0.4;
					break;
				}
				
				if (_sound !== null) _soundChannel = _sound.play(0, 0, _soundTransform);
			}
		}
		
		public function set paused(b:Boolean):void
		{
			if (b == true)
			{
				if (isPlay == false)
				{
					isPlay = true;
					playMainSound(_mainSoundLastPosition);
				}
			}
			
			if (b == false)
			{
				if (isPlay == true)
				{
					isPlay = false;
					_soundChannel.stop();
					_mainSoundLastPosition = _mainSoundChanel.position;
					_mainSoundChanel.stop();
				}
			}
		}
		
		private function playMainSound(pos:int):void 
		{
			_mainSoundChanel = _mainSound.play(pos, 0, _mainTransform);
			_mainSoundChanel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);			
		}
		
		public static function getInstance():SoundManager
		{
			return (_instance == null) ? new SoundManager() : _instance;
		}
		
	}

}