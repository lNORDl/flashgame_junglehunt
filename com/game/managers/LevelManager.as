package com.game.managers 
{
	import com.game.levels.BasicLevel;
	import com.game.levels.Level1;
	import com.game.levels.Level10;
	import com.game.levels.Level11;
	import com.game.levels.Level12;
	import com.game.levels.Level13;
	import com.game.levels.Level14;
	import com.game.levels.Level15;
	import com.game.levels.Level16;
	import com.game.levels.Level17;
	import com.game.levels.Level18;
	import com.game.levels.Level19;
	import com.game.levels.Level2;
	import com.game.levels.Level20;
	import com.game.levels.Level21;
	import com.game.levels.Level22;
	import com.game.levels.Level23;
	import com.game.levels.Level24;
	import com.game.levels.Level25;
	import com.game.levels.Level26;
	import com.game.levels.Level27;
	import com.game.levels.Level28;
	import com.game.levels.Level29;
	import com.game.levels.Level3;
	import com.game.levels.Level30;
	import com.game.levels.Level4;
	import com.game.levels.Level5;
	import com.game.levels.Level6;
	import com.game.levels.Level7;
	import com.game.levels.Level8;
	import com.game.levels.Level9;
	import com.game.objects.Kind;
	import com.game.screens.ScreenGameLevel;
	import flash.net.SharedObject;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class LevelManager extends Object
	{
		public static const MAX_LEVEL:int = 30;
		
		public var unlockLevel:int = 30;
		public var completeLevel:int = 29;
		
		public var completeType:Array = ["BRONZE", "BRONZE", "BRONZE", "BRONZE", "BRONZE",
										 "BRONZE", "BRONZE", "BRONZE", "BRONZE", "BRONZE",
										 "BRONZE", "BRONZE", "BRONZE", "BRONZE", "BRONZE",
										 "BRONZE", "BRONZE", "BRONZE", "BRONZE", "BRONZE",
										 "BRONZE", "BRONZE", "BRONZE", "BRONZE", "BRONZE",
										 "BRONZE", "BRONZE", "BRONZE", "BRONZE", "BRONZE"];
		//public var completeType:Array = [];
		
		public var currentLevel:BasicLevel;
		
		private static var _instance:LevelManager;
		
		private static var Save:SharedObject = SharedObject.getLocal("JungleHunt1SaveData");
		
		public function LevelManager() 
		{
			_instance = this;
		}
		
		public function init():void
		{
			//if (Save.data.firstInit == undefined)
			//{
				//unlockLevel = 1;
				//completeLevel = 0;
				//completeType = [];
			//}
			//else
			//{
				//unlockLevel = Save.data.unlockLevel;
				//completeLevel = Save.data.completeLevel;
				//completeType = Save.data.completeType;
			//}
		}
		
		public function update():void
		{
			if (currentLevel !== null)
			{
				currentLevel.update();
			}
		}
		
		public function createLevel(num:int):BasicLevel
		{
			switch(num)
			{
				case 1:
				currentLevel = new Level1();
				break;	
				
				case 2:
				currentLevel = new Level2();
				break;	
				
				case 3:
				currentLevel = new Level3();
				break;		
				
				case 4:
				currentLevel = new Level4();
				break;		
				
				case 5:
				currentLevel = new Level5();
				break;		
				
				case 6:
				currentLevel = new Level6();
				break;	
				
				case 7:
				currentLevel = new Level7();
				break;	
				
				case 8:
				currentLevel = new Level8();
				break;	
				
				case 9:
				currentLevel = new Level9();
				break;		
				
				case 10:
				currentLevel = new Level10();
				break;		
				
				case 11:
				currentLevel = new Level11();
				break;	
				
				case 12:
				currentLevel = new Level12();
				break;		
				
				case 13:
				currentLevel = new Level13();
				break;	
				
				case 14:
				currentLevel = new Level14();
				break;		
				
				case 15:
				currentLevel = new Level15();
				break;		
				
				case 16:
				currentLevel = new Level16();
				break;	
				
				case 17:
				currentLevel = new Level17();
				break;		
				
				case 18:
				currentLevel = new Level18();
				break;	
				
				case 19:
				currentLevel = new Level19();
				break;	
				
				case 20:
				currentLevel = new Level20();
				break;	
				
				case 21:
				currentLevel = new Level21();
				break;	
				
				case 22:
				currentLevel = new Level22();
				break;	
				
				case 23:
				currentLevel = new Level23();
				break;	
				
				case 24:
				currentLevel = new Level24();
				break;	
				
				case 25:
				currentLevel = new Level25();
				break;	
				
				case 26:
				currentLevel = new Level26();
				break;	
				
				case 27:
				currentLevel = new Level27();
				break;	
				
				case 28:
				currentLevel = new Level28();
				break;	
				
				case 29:
				currentLevel = new Level29();
				break;	
				
				case 30:
				currentLevel = new Level30();
				break;
			}
			return currentLevel;
		}
		
		public function levelComplete(numLevel:int, starType:String):void
		{	
			if (numLevel == unlockLevel)
			{
				unlockLevel ++;
				completeLevel ++;
				completeType[numLevel - 1] = starType;
			}
			else
			{
				var _oldType:String = completeType[numLevel - 1];
				if (getStarTypeMass(_oldType) < getStarTypeMass(starType))
				{
					completeType[numLevel - 1] = starType;
				}
			}
			
			//saveData();
		}
		
		private function saveData():void 
		{
			Save.data.firstInit = true;
			Save.data.unlockLevel = unlockLevel;
			Save.data.completeLevel = completeLevel;
			Save.data.completeType = completeType;
			//Save.flush();
		}
		
		private function getStarTypeMass(type:String):int
		{
			switch(type)
			{
				case "BRONZE":
				return 1;
				
				case "SILVER":
				return 2;
				
				case "GOLD":
				return 3;
			}
			return 0;
		}
		
		public function destroyLevel():void
		{
			if (currentLevel !== null)
			{
				currentLevel.destroy();
				currentLevel = null;
			}
		}
		
		public static function getInstance():LevelManager
		{
			return (_instance == null) ? new LevelManager() : _instance;
		}
		
	}

}