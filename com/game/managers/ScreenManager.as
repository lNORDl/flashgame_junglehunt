package com.game.managers 
{
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import com.game.screens.ScreenAbout;
	import com.game.screens.ScreenGameLevel;
	import com.game.screens.ScreenLevelMenu;
	import com.game.screens.ScreenMainMenu;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class ScreenManager extends Object
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var currentScreen:BasicScreen;
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------	
		private static var _instance:ScreenManager;
		
		private var _nextScreen:BasicScreen;
		
		private var _windows:Array = [];
		
		
		//---------------------------------------
		public function ScreenManager() 
		{
			_instance = this;
		}
		
		public function init():void
		{
			
		}
		
		public function update():void
		{
			if (currentScreen !== null && _nextScreen !== null)
			{
				if (currentScreen.canFadeOut)
				{
					currentScreen.fadeOut();
				}
				
				if (currentScreen.canDestroy)
				{
					currentScreen.destroy();
					currentScreen = null;
				}
			}
			
			if (currentScreen == null && _nextScreen !== null)
			{
				currentScreen = _nextScreen;
				currentScreen.init();
				_nextScreen = null;
			}
			
			if (currentScreen !== null)
			{
				currentScreen.update();
			}			
		}
		
		public function setScreen(tag:int):BasicScreen
		{
			if (_nextScreen == null)
			{			
				switch(tag)
				{
					case Kind.SCREEN_MAINMENU:
						_nextScreen = ScreenMainMenu.create();
					break;
					
					case Kind.SCREEN_LEVELMENU:
						_nextScreen = ScreenLevelMenu.create();
					break;	
					
					case Kind.SCREEN_GAMELEVEL:
						_nextScreen = ScreenGameLevel.create();
					break;		
					
					case Kind.SCREEN_ABOUT:
						_nextScreen = ScreenAbout.create();
					break;	
				}	
				
				return _nextScreen;
			}
			else
			{
				trace("Очередь экранов заполненеа");
				return null;
			}
		}	
		
		public static function getInstance():ScreenManager
		{
			return (_instance == null) ? new ScreenManager() : _instance;
		}
	}

}