package com.game.windows 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.buttons.ButtonMenu;
	import com.game.buttons.ButtonNext;
	import com.game.buttons.ButtonRestart;
	import com.game.levels.BasicLevel;
	import com.game.managers.LevelManager;
	import com.game.managers.SoundManager;
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import flash.display.MovieClip;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WindowEndLevel extends BasicWindow
	{
		
		private var _endLevelPanel:MovieClip;
		private var _soundManager:SoundManager = SoundManager.getInstance();
		
		public function WindowEndLevel() 
		{
			
		}
		
		override public function init(screen:BasicScreen):void
		{
			myTag = Kind.WINDOW_ENDLEVEL;
			_myActor = new AntActor();
			_myActor.addAnimFromCache("WindowEndLevel_mc", null, true);
			
			_myPosition = new Point(300, 200);
			
			if (_levelManager.currentLevel.isComplete) 
			{
				_levelManager.levelComplete(_levelManager.currentLevel.numLevel, _levelManager.currentLevel.starType);
			}
			
			super.init(screen);
		}
		
		public static function create(screen:BasicScreen):BasicWindow
		{
			var _window:BasicWindow = new WindowEndLevel();
			_window.init(screen);
			
			return _window;
		}
		
		override protected function createInterface():void
		{
			_endLevelPanel = new EndLevelPanel_mc();
			addChild(_endLevelPanel);
			
			_endLevelPanel.x = 0;
			_endLevelPanel.y = -43;
			
			var _level:BasicLevel = _levelManager.currentLevel;
			
			var _buttonRestar:BasicButton;
			if (_level.isComplete)
			{
				_endLevelPanel.TextTypeEnd.text = "Complete";
				
				_soundManager.playSound(Kind.SOUND_LEVELCOMPLETE);
				
				if (_level.numLevel == LevelManager.MAX_LEVEL)
				{
					_buttonRestar = ButtonRestart.create(this, new Point(37, 15));
				}
				else
				{
					var _buttonNext:BasicButton = ButtonNext.create(this, new Point(37, 15));
				}
			}
			else
			{
				_endLevelPanel.TextTypeEnd.text = "Failed";
				_buttonRestar = ButtonRestart.create(this, new Point(37, 15));
			}
			
			_endLevelPanel.TextCurrentStar.text = _level.currentStar.toString();
			_endLevelPanel.TextMaxStar.text = _level.maxStar.toString();
			_endLevelPanel.Star.gotoAndStop(_level.starType);
			
			var _buttonMenu:BasicButton = ButtonMenu.create(this, new Point(-37, 15));
		}
		
		override public function update():void
		{	
			
			
			super.update();
		}
		
		override public function destroy():void
		{
			removeChild(_endLevelPanel);
			_endLevelPanel = null;
			
			
			super.destroy();
		}
		
		override protected function onFadeOutComplete():void
		{		
			super.onFadeOutComplete();
			
		}
		
	}

}