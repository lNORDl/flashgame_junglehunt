package com.game.windows 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.Game;
	import com.game.managers.LevelManager;
	import com.game.managers.ScreenManager;
	import com.game.screens.BasicScreen;
	import com.game.Universe;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicWindow extends Sprite
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var canUse:Boolean = false;		
		public var myTag:int;
		
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		protected var _game:Game = Game.getInstance();
		protected var _screenManager:ScreenManager = ScreenManager.getInstance();
		protected var _levelManager:LevelManager = LevelManager.getInstance();
		protected var _universe:Universe = Universe.getInstance();
		
		protected var _myActor:AntActor;
		protected var _myScreen:BasicScreen;
		protected var _myPosition:Point;
		
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		private var _myContainer:Sprite;
		
		private var _isFadeIn:Boolean = false;
		private var _isFadeOut:Boolean = false;
		
		private var _buttons:Array = [];
		
		
		public function BasicWindow() 
		{
			
		}
		
		public function init(screen:BasicScreen):void
		{		
			_myScreen = screen;
			
			_myContainer = _game.windowContainer;
			
			if (_myActor !== null)
			{
				addChild(_myActor);
			}
			
			if (_myPosition !== null)
			{
				this.x = _myPosition.x;
				this.y = _myPosition.y;
			}
			
			_myContainer.addChild(this);
			_myScreen.addWindow(this);
			
			createInterface();
			
			fadeIn();
		}
		
		public function update():void
		{
			
		}
		
		public function destroy():void
		{			
			for (var i:int = 0; i < _buttons.length; i++)
			{
				(_buttons[i] as BasicButton).destroy();
			}
			
			if (_myActor !== null)
			{
				_myActor.free();
				_myActor = null;
			}
			_myContainer.removeChild(this);
			_myContainer = null;
			_myScreen = null;
			_game = null;
			_screenManager = null;
			_levelManager = null;
			_buttons = null;
			_universe = null;
		}
		
		protected function createInterface():void
		{
			
		}
		
		public function fadeIn():void
		{
			_isFadeIn = true;
			
			this.alpha = 0;
			TweenLite.to(this, 0.5, {alpha:1, onComplete:onFadeInComplete});
		}
		public function fadeOut():void
		{			
			_isFadeOut = true;
			canUse = false;
			
			TweenLite.to(this, 0.5, {alpha:0, onComplete:onFadeOutComplete});
		}
		
		protected function onFadeInComplete():void
		{
			_isFadeIn = false;
			canUse = true;
		}
		protected function onFadeOutComplete():void
		{
			_isFadeOut = false;
		}	
		
		public function addButton(button:BasicButton):void
		{
			_buttons.push(button);	
		}		
	}

}