package com.game.windows 
{
	import com.antkarlov.animation.AntActor;
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import flash.display.MovieClip;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WindowStartLevel extends BasicWindow
	{
		
		private var _startLevelPanel:MovieClip;
		private var _liveTime:int = 20;
		
		public function WindowStartLevel() 
		{
			
		}
		
		override public function init(screen:BasicScreen):void
		{
			myTag = Kind.WINDOW_STARTLEVEL;
			_myActor = new AntActor();
			_myActor.addAnimFromCache("WindowStartLevel_mc", null, true);
			
			_myPosition = new Point(300, 200);
			
			
			super.init(screen);
		}
		
		public static function create(screen:BasicScreen):BasicWindow
		{
			var _window:BasicWindow = new WindowStartLevel();
			_window.init(screen);
			
			return _window;
		}
		
		override protected function createInterface():void
		{
			trace("Interface Created");
			
			_startLevelPanel = new StartLevelPanel_mc();
			addChild(_startLevelPanel);
			
			_startLevelPanel.x = 0;
			_startLevelPanel.y = 0;
			
			_startLevelPanel.TextNumLevel.text = _levelManager.currentLevel.numLevel.toString();
		}
		
		override public function update():void
		{	
			if (canUse)
			{
				_levelManager.currentLevel.canPlay = false;
				
				if (_liveTime == 0)
				{
					fadeOut();
				}
				else if (_liveTime > 0)
				{
					_liveTime --;
				}
			}
			
			
			super.update();
		}
		
		override public function destroy():void
		{
			removeChild(_startLevelPanel);
			_startLevelPanel = null;
			
			
			super.destroy();
		}
		
		override protected function onFadeOutComplete():void
		{		
			super.onFadeOutComplete();
			
			
			_levelManager.currentLevel.canPlay = true;
			_universe.canContact = true;
			_myScreen.removeWindow(this);
		}
		
	}

}