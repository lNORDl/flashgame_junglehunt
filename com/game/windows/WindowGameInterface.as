package com.game.windows 
{
	import com.antkarlov.animation.AntActor;
	import com.game.buttons.BasicButton;
	import com.game.buttons.ButtonMenu;
	import com.game.buttons.ButtonRestart;
	import com.game.buttons.ButtonSound;
	import com.game.managers.SoundManager;
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WindowGameInterface extends BasicWindow
	{
		public var count:int = 10;
		
		private var _woodDartPanel:MovieClip;
		private var _ironDartPanel:MovieClip;
		private var _bombDartPanel:MovieClip;
		private var _targetPanel:MovieClip;
		
		private var _soundManager:SoundManager = SoundManager.getInstance();
		
		private var _firstSet:Boolean = true;
		
		public function WindowGameInterface() 
		{
			
		}
		
		override public function init(screen:BasicScreen):void
		{
			myTag = Kind.WINDOW_GAMEINTERFACE;
			_myActor = new AntActor();
			_myActor.addAnimFromCache("WindowGameInterface_mc", null, true);
			
			_myPosition = new Point(0, 417);
			_firstSet = true;
			
			
			super.init(screen);
		}
		
		public static function create(screen:BasicScreen):BasicWindow
		{
			var _window:BasicWindow = new WindowGameInterface();
			_window.init(screen);
			
			return _window;
		}
		
		override protected function createInterface():void
		{
			_woodDartPanel = new WoodDartPanel_mc();
			addChild(_woodDartPanel);
			_ironDartPanel = new IronDartPanel_mc();
			addChild(_ironDartPanel);
			_bombDartPanel = new BombDartPanel_mc();
			addChild(_bombDartPanel);
			_targetPanel = new TargetPanel_mc();
			addChild(_targetPanel);
			
			_woodDartPanel.x = 38;
			_woodDartPanel.y = 13;			
			_ironDartPanel.x = 116;
			_ironDartPanel.y = 13;			
			_bombDartPanel.x = 196;
			_bombDartPanel.y = 13;
			_targetPanel.x = 314;
			_targetPanel.y = 15;
			
			_woodDartPanel.addEventListener(MouseEvent.CLICK, wDcl);
			_ironDartPanel.addEventListener(MouseEvent.CLICK, iDcl);
			_bombDartPanel.addEventListener(MouseEvent.CLICK, bDcl);
			
			var _buttonMenu:BasicButton = ButtonMenu.create(this, new Point(430, 15));
			var _buttonRestart:BasicButton = ButtonRestart.create(this, new Point(510, 15));
			var _buttonSound:BasicButton = ButtonSound.create(this, new Point(570, 15));
		}
		
		override public function destroy():void
		{
			_woodDartPanel.removeEventListener(MouseEvent.CLICK, wDcl);
			_ironDartPanel.removeEventListener(MouseEvent.CLICK, iDcl);
			_bombDartPanel.removeEventListener(MouseEvent.CLICK, bDcl);
			
			removeChild(_woodDartPanel);
			removeChild(_ironDartPanel);
			removeChild(_bombDartPanel);
			removeChild(_targetPanel);
			
			_woodDartPanel = null;
			_ironDartPanel = null;
			_bombDartPanel = null;
			_targetPanel = null;
			
			
			super.destroy();
		}
		
		
		private function wDcl(e:MouseEvent):void 
		{
			if (_levelManager.currentLevel.canPlay) _levelManager.currentLevel.setCurrentDart(Kind.TAG_WOOD_DART);
		}		
		private function iDcl(e:MouseEvent):void 
		{
			if(_levelManager.currentLevel.canPlay) _levelManager.currentLevel.setCurrentDart(Kind.TAG_IRON_DART);
		}		
		private function bDcl(e:MouseEvent):void 
		{
			if(_levelManager.currentLevel.canPlay) _levelManager.currentLevel.setCurrentDart(Kind.TAG_BOMB_DART); 
		}
		
		public function setDartCount(tag:int, count:int):void
		{
			switch(tag)
			{
				case Kind.TAG_WOOD_DART:
				_woodDartPanel.Text.text = count.toString();
				break;
				
				case Kind.TAG_IRON_DART:
				_ironDartPanel.Text.text = count.toString();
				break;
				
				case Kind.TAG_BOMB_DART:
				_bombDartPanel.Text.text = count.toString();
				break;
			}
		}
		
		public function setCurrentDart(tag:int):void
		{
			if (_firstSet) _firstSet = false;
			else _soundManager.playSound(Kind.SOUND_BUTTONCLICK);
			
			_woodDartPanel.Bg.gotoAndStop("WAIT");
			_ironDartPanel.Bg.gotoAndStop("WAIT");
			_bombDartPanel.Bg.gotoAndStop("WAIT");
			
			switch(tag)
			{
				case Kind.TAG_WOOD_DART:
				_woodDartPanel.Bg.gotoAndStop("USING");
				break;
				
				case Kind.TAG_IRON_DART:
				_ironDartPanel.Bg.gotoAndStop("USING");
				break;
				
				case Kind.TAG_BOMB_DART:
				_bombDartPanel.Bg.gotoAndStop("USING");
				break;
			}			
		}
		
		public function setMaxBomb(count:int):void
		{
			_targetPanel.TextMaxBomb.text = count.toString();
		}
		
		public function setCurrentBomb(count:int):void
		{
			_targetPanel.TextCurrentBomb.text = count.toString();
			_targetPanel.BombGlow.gotoAndPlay("ADD");
		}
		
		public function setCurrentStar(count:int):void
		{
			_targetPanel.TextCurrenStar.text = count.toString();
			_targetPanel.StarGlow.gotoAndPlay("ADD");
		}	
		
		public function setMaxStar(count:int):void
		{
			_targetPanel.TextMaxStar.text = count.toString();
		}
		
		public function setStarType(type:String):void
		{
			switch(type)
			{
				case "SILVER":
					_targetPanel.Star.gotoAndPlay("SILVER_STAR");
				break;
				
				case "GOLD":
					_targetPanel.Star.gotoAndPlay("GOLD_STAR");
				break;
			}
		}
		
	}

}