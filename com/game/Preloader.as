package com.game 
{
	import com.greensock.TweenLite;
	import flash.display.*;
	import flash.text.*;
	import flash.events.*;
	
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	
	public class Preloader extends MovieClip
	{
		public static const ENTRY_FRAME:Number = 3;
		public static const DOCUMENT_CLASS:String = "com.game.App";
		
		private var progressBar:MovieClip;
		private var playBtn:MovieClip;
		private var tT:MovieClip;
		
		public function Preloader() 
		{
			super();
			stop();
			
			if (atHome(["local", "fgl.com", "www.fgl.com", "flashgamelicense.com", "www.flashgamelicense.com", "www.easyflash.org"])) 
			{
			   init();
			} 
			else 
			{				
			   trace("Bad Url!!!");
			}	
		}
		
		private function init():void 
		{			
			progressBar = getChildByName("pBar") as MovieClip;
			progressBar.Mask.scaleX = 0;
			progressBar.alpha = 1;
			
			playBtn = getChildByName("PlayBtn") as MovieClip;
			playBtn.alpha = 0;
			
			tT = getChildByName("TT") as MovieClip;
			tT.alpha = 1;
			
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			loaderInfo.addEventListener(Event.COMPLETE, completeHandler);			
		}
		
		private function progressHandler(event:ProgressEvent):void 
		{
			var loaded:uint = event.bytesLoaded;
			var total:uint = event.bytesTotal;
			progressBar.Mask.scaleX = loaded/total;
		}
		
		private function completeHandler(event:Event):void 
		{			
			TweenLite.to(playBtn, 1, {alpha:1, onComplete:fadeInComplete});
		}
		
		private function fadeInComplete():void
		{
			playBtn.buttonMode = true;			
			playBtn.addEventListener(MouseEvent.CLICK, cl);	
			playBtn.addEventListener(MouseEvent.MOUSE_OUT, mu);
			playBtn.addEventListener(MouseEvent.MOUSE_OVER, mo);
		}
		
		private function mu(e:MouseEvent):void 
		{
			playBtn.gotoAndStop("OUT");
		}
		
		private function mo(e:MouseEvent):void 
		{
			playBtn.gotoAndStop("OVER");
		}
		
		private function cl(e:MouseEvent):void 
		{
			playBtn.gotoAndStop("CLICK");			
			
			playBtn.removeEventListener(MouseEvent.CLICK, cl);
			playBtn.removeEventListener(MouseEvent.MOUSE_OUT, mu);
			playBtn.removeEventListener(MouseEvent.MOUSE_OVER, mo);
			
			TweenLite.to(tT, 0.4, { alpha:0, 
			onComplete:function ff():void
			{
				TweenLite.to(progressBar, 0.3, { alpha:0, 
				onComplete:function fff():void
				{
					TweenLite.to(playBtn, 0.2, { alpha:0, 
					onComplete:function ffff():void
					{
						play();
						
						addEventListener(Event.ENTER_FRAME, enterFrameHandler);	
					}
					});
				}
				});
			}
			});			
		}
		
		private function enterFrameHandler(event:Event):void 
		{
			if (currentFrame >= Preloader.ENTRY_FRAME) 
			{
				removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
				stop();
				main();
			}
		}
		
		private function main():void 
		{			
			tT = null;
			playBtn = null;
			progressBar = null;
			
			var programClass:Class = loaderInfo.applicationDomain.getDefinition(Preloader.DOCUMENT_CLASS) as Class;
			var program:Sprite = new programClass() as Sprite;
			addChild(program);
		}	
		
      /**
       * Возвращает домен на котором размещена флешка.
       * <p>Внимание: Если флешка размещена на домене второго или третьего уровня то вернется имя домена первого уровня,
       * то есть если ваша игра размещена на "http://cache.armorgames.com/", то результат будет "armorgames.com".</p>
       * 
       * @return      Вернет local если флешка запущена на локальном компьютере или адрес домена первого уровня "domain.com".
       */
      protected function getHome():String
      {
         var url:String = loaderInfo.loaderURL;
         var urlStart:Number = url.indexOf("://") + 3;
         var urlEnd:Number = url.indexOf("/", urlStart);
         var home:String = url.substring(urlStart, urlEnd);
         var LastDot:Number = home.lastIndexOf(".") - 1;
         var domEnd:Number = home.lastIndexOf(".", LastDot) + 1;
         home = home.substring(domEnd, home.length);
         return (home == "") ? "local" : home;
      }
      
      /**
       * Реализация элементарного сайтлока. Пример использования:
       * <p><code>if (atHome([ "local", "mygreatsite.com" ])) {<br>
       *    // ура! можно играть<br>
       * } else {<br>
       *    // нельзя играть<br>
       * }</code></p>
       * 
       * @param   aHomes    Список доменов на которых флешке разрешено находится.
       * @return      Возвращает true если флешка находится на одном из разрешенных доменов.
       */
      protected function atHome(aHomes:Array):Boolean
      {
         return (aHomes.indexOf(getHome()) > -1) ? true : false;
      }
	}

}