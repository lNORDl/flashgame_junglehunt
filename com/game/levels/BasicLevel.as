package com.game.levels 
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.Joints.b2DistanceJoint;
	import Box2D.Dynamics.Joints.b2DistanceJointDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.antkarlov.animation.AntActor;
	import com.framework.math.Amath;
	import com.game.controllers.ObjectController;
	import com.game.Game;
	import com.game.managers.LevelManager;
	import com.game.managers.ScreenManager;
	import com.game.managers.SoundManager;
	import com.game.objects.BasicObject;
	import com.game.objects.blades.IronBlade;
	import com.game.objects.blades.WoodBlade;
	import com.game.objects.bombs.BlackBomb;
	import com.game.objects.bombs.BlackBombBox;
	import com.game.objects.Hero;
	import com.game.objects.Kind;
	import com.game.objects.pitchers.Pitcher1;
	import com.game.objects.pitchers.Pitcher2;
	import com.game.objects.ropes.BasicRope;
	import com.game.objects.ropes.IronRope1;
	import com.game.objects.ropes.ThreadRope1;
	import com.game.objects.ropes.ThreadRope2;
	import com.game.objects.triggers.TriggerAddBombDart;
	import com.game.objects.triggers.TriggerAddIronDart;
	import com.game.objects.triggers.TriggerAddWoodDart;
	import com.game.objects.triggers.TriggerShootBombDart;
	import com.game.objects.triggers.TriggerShootIronDart;
	import com.game.objects.triggers.TriggerShootWoodDart;
	import com.game.objects.walls.woodPlanks.WoodBox1;
	import com.game.objects.walls.woodPlanks.WoodPlank1;
	import com.game.objects.walls.woodPlanks.WoodPlank2;
	import com.game.objects.walls.woodPlanks.WoodPlank3;
	import com.game.objects.walls.woodWalls.Pivot;
	import com.game.objects.walls.woodWalls.WoodWall1;
	import com.game.objects.walls.woodWalls.WoodWall2;
	import com.game.objects.walls.woodWalls.WoodWall3;
	import com.game.objects.walls.woodWalls.WoodWall4;
	import com.game.objects.walls.woodWalls.WoodWall5;
	import com.game.screens.BasicScreen;
	import com.game.screens.ScreenGameLevel;
	import com.game.Universe;
	import com.game.windows.BasicWindow;
	import com.game.windows.WindowEndLevel;
	import com.game.windows.WindowGameInterface;
	import fl.motion.ITween;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicLevel extends Sprite
	{
		public var numLevel:int;
		public var canPlay:Boolean;
		
		public var woodDartCount:int;
		public var ironDartCount:int;
		public var bombDartCount:int;
		
		public var currentDart:int;
		
		public var maxStar:int;
		public var currentStar:int;
		public var starType:String;
		
		public var maxBomb:int;
		public var currentBomb:int;
		
		public var isComplete:Boolean;
		
		protected var _overGround:Sprite;
		protected var _physicsGround:Sprite;
		
		private var _game:Game = Game.getInstance();
		private var _universe:Universe = Universe.getInstance();
		private var _screenManager:ScreenManager = ScreenManager.getInstance();
		
		private var _myScreen:ScreenGameLevel;
		private var _myInterface:WindowGameInterface;
		
		private var _hero:Hero;
		
		private var RATIO:int = Universe.RATIO;
		
		private var _backGround:AntActor;
		
		private var _isEnd:Boolean = false;
		private var _endLevelWindow:WindowEndLevel;
		
		private var _isExists:Boolean = true;
		
		
		//------------------------------------------------------------------------
		public function BasicLevel() 
		{
			
		}
		
		public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{			
			_myScreen = myScreen as ScreenGameLevel;
			_myInterface = myInterface as WindowGameInterface;
			
			_backGround = new AntActor();
			_backGround.addAnimFromCache("LevelBackGround_mc", null, true);
			_backGround.x = 10;
			_backGround.y = -20;
			addChild(_backGround);
			
			maxStar = 0;
			currentStar = 0;
			
			maxBomb = 0;
			currentBomb = 0;
			
			_isExists = true;
			
			if (_physicsGround !== null)
			{
				currentDart = Kind.TAG_WOOD_DART;
				
				createPhysicsObjects();
			}
			
			if (_overGround !== null)
			{
				_game.ogContainer.addChild(_overGround);
			}
			
			_myScreen.addChild(this);
			
			_myInterface.setDartCount(Kind.TAG_WOOD_DART, woodDartCount);
			_myInterface.setDartCount(Kind.TAG_IRON_DART, ironDartCount);
			_myInterface.setDartCount(Kind.TAG_BOMB_DART, bombDartCount);
			
			setCurrentDart(Kind.TAG_WOOD_DART);
			
			_myInterface.setCurrentStar(currentStar);
			_myInterface.setMaxStar(maxStar);
			_myInterface.setMaxBomb(maxBomb);
			_myInterface.setCurrentBomb(currentBomb);
			
			starType = "BRONZE";
			
			isComplete = false;
		}
		
		private function createPhysicsObjects():void 
		{
			var _num:Number = 0;
			var _dNum:Number = 0;
			var isRot:Boolean = false;
			
			var n:int = _physicsGround.numChildren;
			
			for (var i:int = 0; i < n; i++)
			{
				var _object:MovieClip = _physicsGround.getChildAt(i) as MovieClip;
				
				if (_object is Hero_mc)
				{
					_hero = Hero.create(_object.x, _object.y, _object.rotation, this);
					continue;
				} 		
				
				if (_object is BlackBomb_mc)
				{
					BlackBomb.create(_object.x, _object.y, _object.rotation);
					maxBomb ++;
					continue;
				} 				
				if (_object is BlackBombBox_mc)
				{
					BlackBombBox.create(_object.x, _object.y, _object.rotation);
					continue;
				} 
				
				if (_object is WoodWall1_mc)
				{
					WoodWall1.create(_object.x, _object.y, _object.rotation);
					continue;
				} 	
				if (_object is WoodWall2_mc)
				{
					WoodWall2.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				if (_object is WoodWall3_mc)
				{
					WoodWall3.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				if (_object is WoodWall4_mc)
				{
					WoodWall4.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				if (_object is WoodWall5_mc)
				{
					WoodWall5.create(_object.x, _object.y, _object.rotation);
					continue;
				}
				if (_object is Pivot_mc)
				{
					Pivot.create(_object.x, _object.y, _object.rotation);
					continue;
				}
				
				// Планки
				if (_object is WoodPlank1_mc)
				{
					WoodPlank1.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				if (_object is WoodPlank2_mc)
				{
					WoodPlank2.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				if (_object is WoodPlank3_mc)
				{
					WoodPlank3.create(_object.x, _object.y, _object.rotation);
					continue;
				}
				
				// Лезвия
				if (_object is WoodBlade_mc)
				{
					WoodBlade.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				if (_object is IronBlade_mc)
				{
					IronBlade.create(_object.x, _object.y, _object.rotation);
					continue;
				}				
				
				// Кувшины
				if (_object is Pitcher1_mc)
				{
					Pitcher1.create(_object.x, _object.y, _object.rotation);
					maxStar ++;
				}				
				if (_object is Pitcher2_mc)
				{
					Pitcher2.create(_object.x, _object.y, _object.rotation);
					maxStar ++;
				}
				
				// Веревки
				if (_object is ThreadRope1_mc)
				{
					ThreadRope1.create(_object.x, _object.y, _object.rotation);
					continue;
				}						
				if (_object is ThreadRope2_mc)
				{
					ThreadRope2.create(_object.x, _object.y, _object.rotation);
					continue;
				}	
				
				if (_object is IronRope1_mc)
				{
					IronRope1.create(_object.x, _object.y, _object.rotation);
					continue;
				}								
				
				if (_object is WoodBox1_mc)
				{
					WoodBox1.create(_object.x, _object.y, _object.rotation);
					continue;
				}
				
				if (_object is RopeJoint)
				{
					var _object1:BasicObject = null;
					var _object2:BasicObject = null;
					var _objects:Array = _universe.getObjects().objects;
					var _canCreateJoint:Boolean = false;
					
					for (var j:int = 0; j < _objects.length; j++)
					{
						var _obj:BasicObject = _objects[j];
						
						if (_obj.hitTestObject(_object))
						{
							if (_object1 == null)
							{
								_object1 = _obj;
							}
							else if (_object2 == null)
							{
								_object2 = _obj;
							}
						}
						
						if (_object1 !== null && _object2 !== null)
						{
							if (_object1.myBody !== null && _object2.myBody !== null)
							{
							_canCreateJoint = true;
							break;
							}
						}
					}
					
					if (_canCreateJoint)
					{							
						var _jP:b2Vec2 = new b2Vec2();
						var _p:Point = new Point( 0, -5);
						var _nP:Point = Amath.getRotPoint(_p, _object2.rotation);
						_jP.x = _nP.x / RATIO;
						_jP.y = _nP.y / RATIO;
						
						var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();					
						jointDef.Initialize(_object1.myBody, _object2.myBody, new b2Vec2(_object.x / RATIO, _object.y / RATIO));	
						var joint:b2RevoluteJoint = _universe.physics.CreateJoint(jointDef) as b2RevoluteJoint;	
						
						var jointDef1:b2DistanceJointDef = new b2DistanceJointDef();
						jointDef1.Initialize(_object1.myBody, _object2.myBody, _object1.myBody.GetWorldCenter(), _object2.myBody.GetWorldCenter());		
						var joint1:b2DistanceJoint = _universe.physics.CreateJoint(jointDef1) as b2DistanceJoint;
						
						var _rope1:BasicRope = _object1 as BasicRope;
						var _rope2:BasicRope = _object2 as BasicRope;
						
						if (_rope1 !== null)
						{
							_rope1.myJoints[_rope1.myJoints.length] = [joint, joint1];
						}
						
						if (_rope2 !== null)
						{
							_rope2.myJoints[_rope2.myJoints.length] = [joint, joint1];
						}
					}
				}				
				
				
				isRot = false;
				if (_object is TriggerShootWoodDart_mc)
				{
					_num = Number(_object.name.substring(1, 3));
					_dNum = Number(_object.name.substring(4, 5));					
					if (_dNum == 1) isRot = true;
					TriggerShootWoodDart.create(_object.x, _object.y, _object.rotation, _num, isRot);	
				}					
				else if (_object is TriggerShootIronDart_mc)
				{
					_num = Number(_object.name.substring(1, 3));
					_dNum = Number(_object.name.substring(4, 5));
					if (_dNum == 1) isRot = true;
					TriggerShootIronDart.create(_object.x, _object.y, _object.rotation, _num, isRot);					
				}					
				else if (_object is TriggerShootBombDart_mc)
				{
					_num = Number(_object.name.substring(1, 3));
					_dNum = Number(_object.name.substring(4, 5));
					if (_dNum == 1) isRot = true;
					TriggerShootBombDart.create(_object.x, _object.y, _object.rotation, _num, isRot);					
				}
				
				if (_object is TriggerAddWoodDart_mc)
				{
					_num = Number(_object.name.substring(1, 3));
					TriggerAddWoodDart.create(_object.x, _object.y, _object.rotation, _num);
				}					
				else if (_object is TriggerAddIronDart_mc)
				{
					_num = Number(_object.name.substring(1, 3));
					TriggerAddIronDart.create(_object.x, _object.y, _object.rotation, _num);
				}					
				else if (_object is TriggerAddBombDart_mc)
				{
					_num = Number(_object.name.substring(1, 3));
					TriggerAddBombDart.create(_object.x, _object.y, _object.rotation, _num);
				}	
				
			}
			
			_physicsGround = null;
		}
		
		public function update():void
		{
			if (canPlay) updateInterface();
		}
		
		public function destroy():void
		{		
			if (_isExists)
			{
				if (_myScreen.contains(this))
				{
					_myScreen.removeChild(this);
					_myScreen = null;
				}
				
				if (_backGround !== null)
				{
					_backGround.free();
					_backGround = null;
				}
				
				var Objects:ObjectController = _universe.getObjects();
				Objects.clear();			
				_universe.clearPhysics();			
				
				if (_overGround !== null)
				{
					_game.ogContainer.removeChild(_overGround);
					_overGround = null;
				}
				
				_hero = null;
				_game = null;
				_screenManager = null;
				_myInterface = null;
				_universe.canContact = false;
				
				_isExists = false;
			}
		}
		
		private function updateInterface():void 
		{
			_myInterface.setDartCount(Kind.TAG_WOOD_DART, woodDartCount);
			_myInterface.setDartCount(Kind.TAG_IRON_DART, ironDartCount);
			_myInterface.setDartCount(Kind.TAG_BOMB_DART, bombDartCount);
			
			//_myInterface.setCurrentDart(currentDart);
		}
		
		private function endLevel():void 
		{
			canPlay = false;				
			_endLevelWindow = WindowEndLevel.create(_myScreen) as WindowEndLevel;
			_isEnd = true;
		}
		
		public function mouseMove(mX:int, mY:int):void
		{
			if (_hero !== null && canPlay)
			{
				_hero.mm(mX, mY);
			}
		}
		
		public function mouseUp():void
		{
			if (_hero !== null && canPlay)
			{
				_hero.mu();
			}
		}
		
		public function keyDown(keyCode:int):void
		{
			var _gameScreen:ScreenGameLevel = null;
			
			if (canPlay)
			{
				switch(keyCode)
				{
					case 90: setCurrentDart(Kind.TAG_WOOD_DART); break;
					case 88: setCurrentDart(Kind.TAG_IRON_DART); break;
					case 67: setCurrentDart(Kind.TAG_BOMB_DART); break;
					
					case 82:
					_gameScreen = _screenManager.setScreen(Kind.SCREEN_GAMELEVEL) as ScreenGameLevel;
					_gameScreen.numLevel = numLevel;
					break;
				}
			}
			
			if (_isEnd && _endLevelWindow.canUse && keyCode == 32 && numLevel !== LevelManager.MAX_LEVEL)
			{
				_gameScreen = _screenManager.setScreen(Kind.SCREEN_GAMELEVEL) as ScreenGameLevel;
				_gameScreen.numLevel = numLevel + 1;
			}
		}
		
		public function setCurrentDart(tag:int):void
		{
			switch(tag)
			{
				case Kind.TAG_WOOD_DART:
				currentDart = Kind.TAG_WOOD_DART;
				break;
				
				case Kind.TAG_IRON_DART:
				currentDart = Kind.TAG_IRON_DART;
				break;
				
				case Kind.TAG_BOMB_DART:
				currentDart = Kind.TAG_BOMB_DART;
				break;
			}	
			
			_myInterface.setCurrentDart(currentDart);
		}
		
		public function addStar():void
		{
			if (canPlay)
			{
				currentStar ++;
				_myInterface.setCurrentStar(currentStar);
				
				var _type:String = "BRONZE";
				var _persent:int = Amath.toPercent(currentStar, maxStar);
				if (_persent == 100)
				{
					_type = "GOLD";
				}
				else if (_persent >= 50)
				{
					_type = "SILVER";
				}
				
				if (starType !== _type)
				{
					starType = _type;
					_myInterface.setStarType(starType);
				}
			}
		}
		
		public function boomBomb():void
		{
			//endLevel();
		}
		
		public function completeBomb():void
		{
			if (canPlay)
			{
				currentBomb ++;
				_myInterface.setCurrentBomb(currentBomb);
				
				if (currentBomb == maxBomb)
				{
					isComplete = true;
					endLevel();
				}
			}
		}
		
	}

}