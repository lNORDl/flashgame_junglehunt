package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level7 extends BasicLevel
	{
		
		public function Level7() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 7;
			
			_overGround = new Level7OverGround_mc();
			_physicsGround = new Level7PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}