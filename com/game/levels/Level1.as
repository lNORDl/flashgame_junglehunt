package com.game.levels 
{
	import com.antkarlov.animation.AntActor;
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Level1 extends BasicLevel
	{
		
		public function Level1() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 1;
			
			_overGround = new Level1OverGround_mc();
			_physicsGround = new Level1PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}