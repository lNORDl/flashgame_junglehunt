package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level26 extends BasicLevel
	{
		
		public function Level26() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 26;
			
			_overGround = new Level26OverGround_mc();
			_physicsGround = new Level26PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}