package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level17 extends BasicLevel
	{
		
		public function Level17() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 17;
			
			_overGround = new Level17OverGround_mc();
			_physicsGround = new Level17PhysicsGround_mc();
			
			woodDartCount = 15;
			ironDartCount = 0;
			bombDartCount = 10;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}