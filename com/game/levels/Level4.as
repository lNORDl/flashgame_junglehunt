package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author ...
	 */
	public class Level4 extends BasicLevel
	{
		
		public function Level4() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 4;
			
			_overGround = new Level4OverGround_mc();
			_physicsGround = new Level4PhysicsGround_mc();
			
			woodDartCount = 20;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}