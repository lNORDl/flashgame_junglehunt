package com.game.levels 
{
	import com.antkarlov.animation.AntActor;
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Level2 extends BasicLevel
	{
		
		public function Level2() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 2;
			
			_physicsGround = new Level2PhysicsGround_mc();
			_overGround = new Level2OverGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}