package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level25 extends BasicLevel
	{
		
		public function Level25() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 25;
			
			_overGround = new Level25OverGround_mc();
			_physicsGround = new Level25PhysicsGround_mc();
			
			woodDartCount = 20;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}