package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level12 extends BasicLevel
	{
		
		public function Level12() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 12;
			
			_overGround = new Level12OverGround_mc();
			_physicsGround = new Level12PhysicsGround_mc();
			
			woodDartCount = 20;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}