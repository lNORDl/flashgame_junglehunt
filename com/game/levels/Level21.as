package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level21 extends BasicLevel
	{
		
		public function Level21() 
		{
			
		}		
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 21;
			
			_overGround = new Level21OverGround_mc();
			_physicsGround = new Level21PhysicsGround_mc();
			
			woodDartCount = 20;
			ironDartCount = 0;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}