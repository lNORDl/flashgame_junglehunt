package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level29 extends BasicLevel
	{
		
		public function Level29() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 29;
			
			_overGround = new Level29OverGround_mc();
			_physicsGround = new Level29PhysicsGround_mc();
			
			woodDartCount = 2;
			ironDartCount = 0;
			bombDartCount = 2;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}