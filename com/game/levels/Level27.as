package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level27 extends BasicLevel
	{
		
		public function Level27() 
		{
			
		}		
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 27;
			
			_overGround = new Level27OverGround_mc();
			_physicsGround = new Level27PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 10;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}