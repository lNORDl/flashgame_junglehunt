package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level19 extends BasicLevel
	{
		
		public function Level19() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 19;
			
			_overGround = new Level19OverGround_mc();
			_physicsGround = new Level19PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 10;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}