package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level16 extends BasicLevel
	{
		
		public function Level16() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 16;
			
			_overGround = new Level16OverGround_mc();
			_physicsGround = new Level16PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 0;
			bombDartCount = 10;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}