package com.game.levels 
{
	import com.game.screens.BasicScreen;
	import com.game.windows.BasicWindow;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class Level9 extends BasicLevel
	{
		
		public function Level9() 
		{
			
		}
		
		override public function init(myScreen:BasicScreen, myInterface:BasicWindow):void
		{
			numLevel = 9;
			
			_overGround = new Level9OverGround_mc();
			_physicsGround = new Level9PhysicsGround_mc();
			
			woodDartCount = 10;
			ironDartCount = 10;
			bombDartCount = 0;
			
			
			super.init(myScreen, myInterface);
		}
		
	}

}