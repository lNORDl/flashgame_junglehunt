package com.game.objects 
{
	import Box2D.Common.Math.b2Vec2;
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.levels.BasicLevel;
	import com.game.managers.LevelManager;
	import com.game.objects.arrows.BombDart;
	import com.game.objects.arrows.IronDart;
	import com.game.objects.arrows.TestArrow;
	import com.game.objects.arrows.WoodDart;
	import com.game.objects.bombs.BlackBomb;
	import com.game.Universe;
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class Hero extends BasicObject
	{		
		public var myLevel:BasicLevel;
		
		private var _isFire:Boolean;
		private var _power:Number = 0;
		private var _maxPower:Number = 15;
		private var _angle:Number = 0;		
		private var _mX:int = 0;
		private var _mY:int = 0;	
		
		//private var _dot:MovieClip;
		
		private var _points:Array = [];
		private var _arrow:TestArrow;
		
		//----------------------------------------------------
		public function Hero() 
		{
			_arrow = TestArrow.create();
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			_isFire = false;
			
			myGroup = Kind.GROUP_HERO;
			myTag = Kind.TAG_HERO;			
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, md);
			
			
			super.init(posX, posY, rot);
			
			
			mySprite.buttonMode = true;
		}
		
		override protected function createMySprite():void
		{
			mySprite = new Hero_mc();	
		}		
		
		public static function create(posX:Number, posY:Number, rot:Number, level:BasicLevel):Hero
		{
			var _hero:Hero = Universe.getInstance().getFromCache(Kind.TAG_HERO) as Hero;
			if (_hero == null) _hero = new Hero();
			_hero.myLevel = level;
			_hero.init(posX, posY, rot);
			
			
			return _hero;
		}
		
		override public function update(delta:Number):void
		{
			if (myLevel.canPlay)
			{
				var _indicator:MovieClip = mySprite.Indicator;				
				
				_angle = Amath.toDegrees(Amath.getAngle(this.x, this.y, _mX, _mY));
				
				if (_isFire)
				{					
					this.rotation = _angle + 180;	
					
					if (Amath.distance(this.x, this.y, _mX, _mY) < 55 / 2)
					{
						_power = 0;
						clearPoints();
					}
					else
					{
						_power = (Amath.distance(this.x, this.y, _mX, _mY) - 55 / 2) / 2;	
					}
					if (_power > _maxPower)
					{
						_power = _maxPower;
					}
					
					var _percent:Number = Amath.toPercent(_power, _maxPower);
					var _frame:int = Amath.fromPercent(_percent, 55);
					_indicator.Mask.x = -55 + _frame;
					
					if(_power > 0) showTrajectory(this.x, this.y, _angle, _power);
				}
				else
				{
					if (_indicator.Mask.x > -55)
					{
						_indicator.Mask.x -= 2;
					}
					else
					{
						_indicator.Mask.x = -55;
					}
				}
			}
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			clearPoints();
			
			this.removeEventListener(MouseEvent.MOUSE_DOWN, md);
			myLevel = null;
			
			var _indicator:MovieClip = mySprite.Indicator;
			_indicator.Mask.x = -55;
			
			
			super.destroy();
		}
		
		private function showTrajectory(posX:int, posY:int, angle:Number, power:Number):void 
		{
			clearPoints();
			
			var _shootPoint:Point = Amath.getRotPoint(new Point( -15, 0), _angle + 180);
			_shootPoint.x += posX;
			_shootPoint.y += posY;
			
			_arrow.init(_shootPoint.x, _shootPoint.y, angle + 180, power);
			
			var _ds:int = 5;
			
			var _it:int = 50 - power * 2;
			
			for (var i:int = 0; i < _it; i++)
			{
				_universe.testPhysics.Step(1 / 35, 1);
				_arrow.update();
				
				var _persent:Number = Amath.toPercent(i, _it);
				
				var _tp:MovieClip = new TPoint_mc();
				_game.gameContainer.addChild(_tp);
				_tp.alpha = 1 - _persent / 100;
				_points.push(_tp);
				_tp.x = _arrow.myBody.GetPosition().x * RATIO;
				_tp.y = _arrow.myBody.GetPosition().y * RATIO;
			}				
		}
		
		private function clearPoints():void
		{
			for (var j:int = 0; j < _points.length; j++)
			{
				_game.gameContainer.removeChild(_points[j]);
				_points[j] = null;
			}
			
			_points = [];			
		}
		
		public function mm(mX:int, mY:int):void
		{
			_mX = mX;
			_mY = mY;			
		}
		
		private function md(e:MouseEvent):void 
		{
			if (myLevel.canPlay)
			{
				_isFire = true;
			}
		}
		
		
		public function mu():void 
		{		
			if (_isFire && _power !== 0)
			{		
				shoot();
				_isFire = false;				
			}
			else
			{
				clearPoints();
				_isFire = false;
			}
		}
		
		private function shoot():void 
		{		
			var _shootPoint:Point = Amath.getRotPoint(new Point( -15, 0), _angle + 180);
			_shootPoint.x += this.x;
			_shootPoint.y += this.y;
			
			var _shoot:Boolean = false;
			
			switch(myLevel.currentDart)
			{
				case Kind.TAG_WOOD_DART:
					if (myLevel.woodDartCount > 0) 
					{
						WoodDart.create(_shootPoint.x, _shootPoint.y, _angle + 180, _power); 
						myLevel.woodDartCount --;
						_shoot = true;
					}
				break;
				
				case Kind.TAG_IRON_DART:
					if (myLevel.ironDartCount > 0)
					{					
						IronDart.create(_shootPoint.x, _shootPoint.y, _angle + 180, _power); 
						myLevel.ironDartCount --;
						_shoot = true;
					}
				break;		
				
				case Kind.TAG_BOMB_DART:
					if (myLevel.bombDartCount > 0) 
					{
						BombDart.create(_shootPoint.x, _shootPoint.y, _angle + 180, _power);  
						myLevel.bombDartCount --;
						_shoot = true;
					}
				break;
			}
			
			if (_shoot)
			{				
				_soundManager.playSound(Kind.SOUND_SHOOTARROW);				
			}
			
			clearPoints();
		}
		
	}

}