package com.game.objects 
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.Joints.b2Joint;
	import Box2D.Dynamics.Joints.b2JointEdge;
	import com.antkarlov.animation.AntActor;
	import com.framework.math.Amath;
	import com.game.Game;
	import com.game.interfaces.IGameObject;
	import com.game.managers.LevelManager;
	import com.game.managers.ScreenManager;
	import com.game.managers.SoundManager;
	import com.game.Universe;
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicObject extends MovieClip implements IGameObject
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var myGroup:uint; // Разновидность - танк, башня, ящик...
		public var myTag:uint; // Вид объекта
		public var isExists:Boolean = false; // Существование
		public var type:String = "";
		
		public var myBody:b2Body; // Физическое тело
		public var mySprite:MovieClip; // Графическое отображение
		public var myActor:AntActor = null;
		
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		protected var _game:Game = Game.getInstance();
		protected var _universe:Universe = Universe.getInstance();		
		protected var _screenManager:ScreenManager = ScreenManager.getInstance();
		protected var _levelManager:LevelManager = LevelManager.getInstance();
		protected var _soundManager:SoundManager = SoundManager.getInstance();
		protected var RATIO:uint = Universe.RATIO;
		
		protected var _myActorName:String;
		
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------		
		public function BasicObject() 
		{
			
		}
		
		public function init(posX:Number, posY:Number, rot:Number):void
		{		
			isExists = true;
			type = "";
			_myActorName = "";
			
			if (mySprite == null)
			{				
				createMySprite();
			}
			
			if (mySprite !== null)
			{
				addChild(mySprite);
				
				this.x = posX;
				this.y = posY;
				this.rotation = rot;
			}
			
			if (myActor == null)
			{
				createMyActor();
			}
			
			if (myActor !== null)
			{
				addChild(myActor);
				
				this.x = posX;
				this.y = posY;
				this.rotation = rot;
			}
			
			if (mySprite !== null || myActor !== null)
			{			
				_game.gameContainer.addChild(this);
				_universe.addObject(this);
			}			
		}
		
		protected function createMySprite():void 
		{
			
		}
		
		protected function createMyActor():void 
		{
			if (_myActorName !== "")
			{
				myActor = new AntActor();
				myActor.addAnimFromCache(_myActorName, null, true);
				myActor.smoothing = true;
			}
		}
		
		public function update(delta:Number):void
		{
			if (myBody !== null && mySprite !== null)
			{
				this.x = myBody.GetPosition().x * RATIO;
				this.y = myBody.GetPosition().y * RATIO;
				this.rotation = Amath.toDegrees(myBody.GetAngle());				
			}
			else if (myBody !== null && myActor !== null)
			{
				this.x = myBody.GetPosition().x * RATIO;
				this.y = myBody.GetPosition().y * RATIO;
				this.rotation = Amath.toDegrees(myBody.GetAngle());				
			}
			
			if (this.x < 0 || this.x > 600 || this.y > 450)
			{
				trace("Out");
				destroy();
			}
		}
		
		public function destroy():void
		{
			if (isExists)
			{
				_game.gameContainer.removeChild(this);
				
				if (mySprite !== null) removeChild(mySprite);
				if (myActor !== null) removeChild(myActor);
				
				if (myBody !== null)
				{										
					_universe.physics.DestroyBody(myBody);
					myBody = null;
				}
				
				_universe.removeObject(this);				
				_universe.addToCache(this);
				
				type = "";
				
				this.x = 0;
				this.y = 0;
				
				isExists = false;
			}
		}		
		
		//public function destroy():void
		//{
			//if (isExists)
			//{
				//_game.gameContainer.removeChild(this);
				//
				//if (mySprite !== null) removeChild(mySprite); mySprite = null;
				//if (myActor !== null) removeChild(myActor); myActor = null;
				//
				//if (myBody !== null)
				//{										
					//_universe.physics.DestroyBody(myBody);
					//myBody = null;
				//}
				//
				//_universe.removeObject(this);				
				//
				//type = "";
				//
				//this.x = -100;
				//this.y = -100;
				//
				//_universe = null;
				//_screenManager = null;
				//_levelManager = null;
				//_soundManager = null;
				//_game = null;
				//
				//isExists = false;
			//}
		//}
	}

}