package com.game.objects.arrows 
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.Joints.b2DistanceJoint;
	import Box2D.Dynamics.Joints.b2DistanceJointDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.objects.BasicObject;
	import com.game.objects.effects.EffectBombDartDestroy;
	import com.game.objects.Kind;
	import com.game.Universe;
	import flash.display.MovieClip;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BombDart extends BasicArrow
	{
		
		public function BombDart() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_DART;
			myTag = Kind.TAG_BOMB_DART;
			
			createMyBody(posX, posY, rot);
			
			var _impulse:Avector = new Avector();
			_impulse.asSpeed(startInpulse, Amath.toRadians(rot));			
			myBody.ApplyImpulse(new b2Vec2(_impulse.x, _impulse.y), myBody.GetWorldCenter());
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new BombDart_mc();	
		}
		
		public static function create(posX:Number, posY:Number, rot:Number, inpulse:Number):void
		{
			var _dart:BombDart = Universe.getInstance().getFromCache(Kind.TAG_BOMB_DART) as BombDart;
			if (_dart == null) _dart = new BombDart();
			_dart.startInpulse = inpulse;
			_dart.init(posX, posY, rot);
		}
		
		override protected function hit():void
		{
			var _range:Number = 50;
			var _aroundObjects:Array = getAroundObjects(_range / RATIO);
			for (var i:int = 0; i < _aroundObjects.length; i++)
			{
				var _object:BasicObject = _aroundObjects[i];
				if (_object.myGroup == Kind.GROUP_WOOD_PLANK || _object.myGroup == Kind.GROUP_THREAD_ROPE || _object.myGroup == Kind.GROUP_IRON_ROPE || _object.myGroup == Kind.GROUP_BOMB)
				{
					var _impulse:Number = _range - Amath.distance(this.x, this.y, _object.x, _object.y);
					if (_impulse < 1) _impulse = 1;
					if (_impulse > 4) _impulse = 4; 
					var _angle:Number = Amath.getAngle(this.x, this.y, _object.x, _object.y);
					var _sp:Avector = new Avector();
					_sp.asSpeed(_impulse, _angle);
					
					_object.myBody.ApplyImpulse(new b2Vec2(_sp.x, _sp.y), _object.myBody.GetWorldCenter());
				}
				
				if (_object.myGroup == Kind.GROUP_PITCHER)
				{
					_object.type = "HITP";
				}
			}
			
			var _crPoint:Point = Amath.getRotPoint(new Point( -10, 0), this.rotation);
			_crPoint.x += this.x;
			_crPoint.y += this.y;
			
			_soundManager.playSound(Kind.SOUND_ARROWEXPLOSION);
			EffectBombDartDestroy.create(_crPoint.x, _crPoint.y, Amath.random(0, 360));	
			
			
			super.hit();
		}			
		
		private function createMyBody(posX:int, posY:int, rot:int):void 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var tipDef:b2PolygonDef = new b2PolygonDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			//----------------------Черенок---------------------------------------
			tipDef.userData = "TipBombDart";
			tipDef.isSensor = true;
			tipDef.density = 0.1;
			tipDef.friction = 0.8;
			tipDef.restitution = 0.1;
			tipDef.SetAsOrientedBox(10 / 2 / RATIO, 4 / 2 / RATIO, new b2Vec2(10 / 2 / RATIO, 0), Amath.toRadians(0));
			tipDef.filter.groupIndex = -1;
			
			polyDef.density = 10;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.SetAsBox(20 / 2 / RATIO, 4 / 2 / RATIO);
			polyDef.filter.groupIndex = -1;
			polyDef.filter.categoryBits = Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_THREAD_ROPE;
			
			bodyDef.isBullet = true;
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			myBody = _universe.physics.CreateBody(bodyDef);
			_tipShape = myBody.CreateShape(tipDef);
			_bodyShape = myBody.CreateShape(polyDef);
			
			myBody.SetMassFromShapes();		
			
			//------------------------Оперение------------------------------------
			polyDef.density = 2;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.SetAsBox(4 / 2 / RATIO, 4 / 2 / RATIO);
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			var _sX:Number = 12 * Math.cos(Amath.toRadians(rot));
			var _sY:Number = 12 * Math.sin(Amath.toRadians(rot));
			bodyDef.position.Set((posX - _sX) / RATIO, (posY - _sY) / RATIO);
			
			_plumageBody = _universe.physics.CreateBody(bodyDef);
			_plumageBody.CreateShape(polyDef);
			_plumageBody.SetMassFromShapes();			
			
			//----------------------Соединение------------------------------------
			
			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();					
			jointDef.Initialize(_plumageBody, myBody, myBody.GetWorldPoint(new b2Vec2(-10 / RATIO, 0)));
			jointDef.lowerAngle = myBody.GetAngle();
			jointDef.upperAngle = myBody.GetAngle();
			jointDef.enableLimit = true;	
			var joint:b2RevoluteJoint = _universe.physics.CreateJoint(jointDef) as b2RevoluteJoint;	
		}
		
		// Возвращает массив всех объектов вокруг объекта в заданном радиусе
		private function getAroundObjects(radius:Number = 3):Array 
		{
			var _aroundObjects:Array = [];
			
			// Запрашиваем все фигуры из физического мира вокруг себя в заданном радиусе
			var aabb:b2AABB = new b2AABB();
			aabb.lowerBound.Set(myBody.GetWorldCenter().x - radius, myBody.GetWorldCenter().y - radius);
			aabb.upperBound.Set(myBody.GetWorldCenter().x + radius, myBody.GetWorldCenter().y + radius);
			var _shapes:Array = [];
			_universe.physics.Query(aabb, _shapes, 20);
			
			for (var i:int = 0; i < _shapes.length; i++)
			{
				var _body:b2Body = (_shapes[i] as b2Shape).GetBody();
				var _object:BasicObject = _body.GetUserData() as BasicObject;
				
				if (_object !== null && _object !== (this as BasicObject))
				{
					if (_aroundObjects.indexOf(_object) == -1)
					{
						_aroundObjects.push(_object);
					}
				}
			}
			
			
			return _aroundObjects;
		}
		
	}

}