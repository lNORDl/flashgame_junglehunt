package com.game.objects.arrows 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.Joints.b2DistanceJoint;
	import Box2D.Dynamics.Joints.b2DistanceJointDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.objects.Kind;
	import com.game.Universe;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.A.N.I.I.L.
	 */
	public class WoodDart extends BasicArrow
	{
		
		public function WoodDart() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_DART;
			myTag = Kind.TAG_WOOD_DART;	
			
			createMyBody(posX, posY, rot);
			
			var _impulse:Avector = new Avector();
			_impulse.asSpeed(startInpulse, Amath.toRadians(rot));			
			myBody.ApplyImpulse(new b2Vec2(_impulse.x, _impulse.y), myBody.GetWorldCenter());
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new WoodDart_mc();
			//trace("Sprite created");
		}
		
		public static function create(posX:Number, posY:Number, rot:Number, inpulse:Number):void
		{
			var _dart:WoodDart = Universe.getInstance().getFromCache(Kind.TAG_WOOD_DART) as WoodDart;
			if (_dart == null) _dart = new WoodDart();
			_dart.startInpulse = inpulse;
			_dart.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:int, posY:int, rot:int):void 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var tipDef:b2PolygonDef = new b2PolygonDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			//----------------------Черенок---------------------------------------
			tipDef.userData = "TipWoodDart";
			tipDef.isSensor = true;
			tipDef.density = 0.1;
			tipDef.friction = 0.8;
			tipDef.restitution = 0.1;
			tipDef.SetAsOrientedBox(10 / 2 / RATIO, 4 / 2 / RATIO, new b2Vec2(10 / 2 / RATIO, 0), Amath.toRadians(0));
			tipDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE;	
			
			polyDef.density = 10;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.SetAsBox(20 / 2 / RATIO, 4 / 2 / RATIO);
			polyDef.filter.groupIndex = -1;
			polyDef.filter.categoryBits = Kind.CATEGORY_IRON_ROPE;	
			
			bodyDef.isBullet = true;
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			myBody = _universe.physics.CreateBody(bodyDef);
			_tipShape = myBody.CreateShape(tipDef);
			_bodyShape = myBody.CreateShape(polyDef);
			
			myBody.SetMassFromShapes();		
			
			//------------------------Оперение------------------------------------
			polyDef.density = 2;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.SetAsBox(4 / 2 / RATIO, 4 / 2 / RATIO);
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			var _sX:Number = 12 * Math.cos(Amath.toRadians(rot));
			var _sY:Number = 12 * Math.sin(Amath.toRadians(rot));
			bodyDef.position.Set((posX - _sX) / RATIO, (posY - _sY) / RATIO);
			
			_plumageBody = _universe.physics.CreateBody(bodyDef);
			_plumageBody.CreateShape(polyDef);
			_plumageBody.SetMassFromShapes();			
			
			//----------------------Соединение------------------------------------
			
			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();					
			jointDef.Initialize(_plumageBody, myBody, myBody.GetWorldPoint(new b2Vec2(-10 / RATIO, 0)));
			jointDef.lowerAngle = myBody.GetAngle();
			jointDef.upperAngle = myBody.GetAngle();
			jointDef.enableLimit = true;	
			var joint:b2RevoluteJoint = _universe.physics.CreateJoint(jointDef) as b2RevoluteJoint;	
		}
		
	}

}