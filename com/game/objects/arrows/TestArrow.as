package com.game.objects.arrows 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.game.Universe;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class TestArrow extends Object
	{
		private static var _universe:Universe = Universe.getInstance();
		private static var RATIO:int = Universe.RATIO;
		
		public var myBody:b2Body;
		private var _plumageBody:b2Body;	
		
		public function TestArrow() 
		{
			
		}
		
		public function init(posX:int, posY:int, rot:int, impulse:Number):void
		{
			if (myBody !== null && _plumageBody !== null)
			{
				_universe.testPhysics.DestroyBody(_plumageBody);
				_universe.testPhysics.DestroyBody(myBody);
				
				_plumageBody = null;
				myBody = null;
			}
			
			createMyBody(posX, posY, rot);
			
			var _impulseX:Number = impulse * Math.cos(Amath.toRadians(rot));
			var _impulseY:Number = impulse * Math.sin(Amath.toRadians(rot));
			
			myBody.ApplyImpulse(new b2Vec2(_impulseX, _impulseY), myBody.GetWorldCenter());		
		}
		
		public static function create():TestArrow
		{
			var _arrow:TestArrow = new TestArrow();
			
			return _arrow;
		}	
		
		//---------Полет стрелы------------------------------------------------------
		public function update():void
		{			
			speedCut(_plumageBody, 1, 0);
		}
		
		private function speedCut(b:b2Body, xk:Number, yk:Number):void 
		{
            var spd,spd2:b2Vec2;
            spd = b.GetLinearVelocity();
            spd2 = b.GetLocalVector(spd);
			
            spd2.x = spd2.x * xk;
            spd2.y = spd2.y * yk;
			
            spd = b.GetWorldVector(spd2);
            b.SetLinearVelocity(spd);			
        }
		//--------------------------------------------------------------------------
		
		private function createMyBody(posX:int, posY:int, rot:int):void 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var tipDef:b2PolygonDef = new b2PolygonDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			tipDef.isSensor = true;
			tipDef.density = 0.1;
			tipDef.friction = 0.8;
			tipDef.restitution = 0.1;
			tipDef.SetAsOrientedBox(10 / 2 / RATIO, 4 / 2 / RATIO, new b2Vec2(10 / 2 / RATIO, 0), Amath.toRadians(0));
			
			polyDef.density = 10;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.SetAsBox(20 / 2 / RATIO, 4 / 2 / RATIO);
			
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			myBody = _universe.testPhysics.CreateBody(bodyDef);
			myBody.CreateShape(tipDef);
			myBody.CreateShape(polyDef);
			myBody.SetMassFromShapes();		
			
			//------------------------Оперение------------------------------------
			polyDef.density = 2;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.SetAsBox(4 / 2 / RATIO, 4 / 2 / RATIO);
			
			bodyDef.angle = Amath.toRadians(rot);
			var _sX:Number = 12 * Math.cos(Amath.toRadians(rot));
			var _sY:Number = 12 * Math.sin(Amath.toRadians(rot));
			bodyDef.position.Set((posX - _sX) / RATIO, (posY - _sY) / RATIO);
			
			_plumageBody = _universe.testPhysics.CreateBody(bodyDef);
			_plumageBody.CreateShape(polyDef);
			_plumageBody.SetMassFromShapes();			
			
			//----------------------Соединение------------------------------------
			
			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();					
			jointDef.Initialize(_plumageBody, myBody, myBody.GetWorldPoint(new b2Vec2(-10 / RATIO, 0)));
			jointDef.lowerAngle = myBody.GetAngle();
			jointDef.upperAngle = myBody.GetAngle();
			jointDef.enableLimit = true;	
			var joint:b2RevoluteJoint = _universe.testPhysics.CreateJoint(jointDef) as b2RevoluteJoint;	
		}
		
	}

}