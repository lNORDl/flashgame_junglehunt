package com.game.objects.arrows 
{
	import Box2D.Collision.Shapes.b2FilterData;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.Joints.b2DistanceJoint;
	import Box2D.Dynamics.Joints.b2DistanceJointDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.game.objects.BasicObject;
	import com.game.objects.Kind;
	import com.game.objects.walls.BasicWall;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicArrow extends BasicObject
	{
		public var startInpulse:Number = 0;
		public var isLive:Boolean = true;
		public var stickBody:b2Body;
		
		protected var _plumageBody:b2Body;
		protected var _tipShape:b2Shape;
		protected var _bodyShape:b2Shape;
		
		private var _canFly:Boolean = true;
		public var isFirsFrame:Boolean = true;
		
		private var _z:int = 0;
		
		public function BasicArrow() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			startInpulse = 0;
			isLive = true;
			stickBody = null;
			_canFly = true;
			isFirsFrame = true;
			_z = 0;
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			if (type == "PLYMB")
			{
				plymb();
			}
			
			if (type == "STICK")
			{
				stick();
			}
			
			if (type == "DESTROY")
			{
				destroy();
			}
			
			if (type == "HIT")
			{
				hit();
			}
			
			if (isLive)
			{
				if (_plumageBody !== null) speedCut(_plumageBody, 1, 0);
			}
			
			if (isFirsFrame)
			{
				if (_z < 2)
				{
					_z ++;
				}
				else
				{
					isFirsFrame = false;
				}
			}
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			isLive = false;
			_tipShape.SetUserData("");
			
			if (_plumageBody !== null) _universe.physics.DestroyBody(_plumageBody); _plumageBody = null;
			stickBody = null;
			_tipShape = null;
			_bodyShape = null;
			
			super.destroy();
		}
		
		private function speedCut(b:b2Body, xk:Number, yk:Number):void 
		{
			var spd,spd2:b2Vec2;
			spd = b.GetLinearVelocity();
			spd2 = b.GetLocalVector(spd);
			
			spd2.x = spd2.x * xk;
			spd2.y = spd2.y * yk;
			
			spd = b.GetWorldVector(spd2);
			b.SetLinearVelocity(spd);
        }
		
		private function plymb():void 
		{
			if (_plumageBody !== null)
			{
				_universe.physics.DestroyBody(_plumageBody);
				_plumageBody = null;
			}
		}
		
		public function stick():void
		{
			_soundManager.playSound(Kind.SOUND_STICKARROW);
			
			isLive = false;
			_canFly = false;
			_tipShape.SetUserData("");
			
			plymb();
			
			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();					
			jointDef.Initialize(stickBody, myBody, myBody.GetWorldCenter());
			jointDef.lowerAngle = myBody.GetAngle();
			jointDef.upperAngle = myBody.GetAngle();
			jointDef.enableLimit = true;	
			var joint:b2RevoluteJoint = _universe.physics.CreateJoint(jointDef) as b2RevoluteJoint;	
			
			myBody.DestroyShape(_bodyShape);
			_bodyShape = null;
			
			type = "";
			
			var _wall:BasicObject = stickBody.GetUserData();
			if(_wall is BasicWall) (_wall as BasicWall).arrows.push(this);
		}
		
		protected function hit():void
		{
			destroy();
		}
		
	}

}