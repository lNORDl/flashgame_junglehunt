package com.game.objects.triggers 
{
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.objects.BasicObject;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicTrigger extends BasicObject
	{
		public var isRot:Boolean = false;
		public var startInpulse:Number = 0;
		public var _woodDartCount:Number = 0;
		public var _ironDartCount:Number = 0;
		public var _bombDartCount:Number = 0;
		
		private var _canDestroy:Boolean = false;
		private var _zd:int = 10;
		
		private var _av:Avector;
		private var _upAngle:Number;
		private var _downAngle:Number;
		private var _nowAngle:Number;
		private var _goUp:Boolean;
		private var _goDown:Boolean;
		
		public function BasicTrigger() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			_canDestroy = false;
			_zd = 10;
			_av = new Avector();
			
			_av.asSpeed(35 / 2, Amath.toRadians(rot - 45));			
			_upAngle = Amath.toDegrees(Amath.getAngle(0, 0, _av.x, _av.y));		
			
			_av.asSpeed(35 / 2, Amath.toRadians(rot + 45));			
			_downAngle = Amath.toDegrees(Amath.getAngle(0, 0, _av.x, _av.y));
			
			_goDown = false;
			_goUp = false;
			
			if (Amath.random(0, 1) == 0)
			{
				_goUp = true;
			}
			else
			{
				_goDown = true;
			}
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			if (isRot)
			{
				if (_goUp)
				{
					this.rotation -= 2;
					
					_av.asSpeed(35 / 2, Amath.toRadians(this.rotation));			
					_nowAngle = Amath.toDegrees(Amath.getAngle(0, 0, _av.x, _av.y));
					
					if (Amath.equal(_upAngle, _nowAngle, 2))
					{
						_goUp = false;
						_goDown = true;
					}
				}				
				else if (_goDown)
				{
					this.rotation += 2;
					
					_av.asSpeed(35 / 2, Amath.toRadians(this.rotation));			
					_nowAngle = Amath.toDegrees(Amath.getAngle(0, 0, _av.x, _av.y));
					
					if (Amath.equal(_downAngle, _nowAngle, 2))
					{
						_goUp = true;
						_goDown = false;
					}
				}
			}
			
			if (_zd > 0) _zd --;
			else _canDestroy = true;
			
			if (type == "HIT")
			{				
				if (_canDestroy)
				{
					hit();
				}
				else
				{
					type = "";
				}
			}
		}
		
		override public function destroy():void
		{
			startInpulse = 0;
			_woodDartCount = 0;
			_ironDartCount = 0;
			_bombDartCount = 0
			isRot = false;
			
			
			super.destroy();
		}
		
		protected function hit():void
		{
			_canDestroy = false;
			destroy();
		}
		
	}

}