package com.game.objects.triggers 
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.objects.arrows.WoodDart;
	import com.game.objects.BasicObject;
	import com.game.objects.effects.EffectTriggerShoot;
	import com.game.objects.Kind;
	import com.game.Universe;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class TriggerShootWoodDart extends BasicTrigger
	{
		
		public function TriggerShootWoodDart() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_TRIGGER;
			myTag = Kind.TAG_TRIGGER_SHOOT_WOOD_DART;			
			
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new TriggerShootWoodDart_mc();	
		}
		
		public static function create(posX:Number, posY:Number, rot:Number, inpulse:Number, issRot:Boolean):void
		{
			var _trigger:TriggerShootWoodDart = Universe.getInstance().getFromCache(Kind.TAG_TRIGGER_SHOOT_WOOD_DART) as TriggerShootWoodDart;
			if (_trigger == null) _trigger = new TriggerShootWoodDart();
			_trigger.startInpulse = inpulse;
			_trigger.isRot = issRot;
			_trigger.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{			
			var bodyDef:b2BodyDef = new b2BodyDef();
			var circleDef:b2CircleDef = new b2CircleDef();
			
			circleDef.isSensor = true;
			circleDef.density = 0;
			circleDef.radius = 35 / 2 / RATIO;
			
			bodyDef.isBullet = true;
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(circleDef);
			
			_myBody.SetMassFromShapes();	
			
			
			return _myBody;
		}		
		
		override protected function hit():void
		{
			EffectTriggerShoot.create(this.x, this.y, this.rotation);
			WoodDart.create(this.x, this.y, this.rotation, startInpulse);
			_soundManager.playSound(Kind.SOUND_SHOOTARROW);
			
			
			super.hit();
		}
	}

}