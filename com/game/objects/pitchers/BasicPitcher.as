package com.game.objects.pitchers 
{
	import com.framework.math.Amath;
	import com.game.objects.BasicObject;
	import com.game.objects.effects.EffectPitcherDestroy;
	import com.game.objects.Kind;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicPitcher extends BasicObject
	{
		
		private var _canDestroy:Boolean = false;
		private var _zd:int = 10;
		
		public function BasicPitcher() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			_zd = 10;
			_canDestroy = false;
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			if (_zd > 0) _zd --;
			else _canDestroy = true;
			
			if (type == "HITP")
			{				
				if (_canDestroy)
				{
					hit();
				}
				else
				{
					type = "";
				}
			}
			
			
			super.update(delta);
		}
		
		protected function hit():void
		{
			_levelManager.currentLevel.addStar();
			EffectPitcherDestroy.create(this.x, this.y, Amath.random(0, 360));
			_canDestroy = false;
			_soundManager.playSound(Kind.SOUND_PITCHERDESTROY);
			
			destroy();
		}
		
	}

}