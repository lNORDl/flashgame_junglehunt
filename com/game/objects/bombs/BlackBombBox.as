package com.game.objects.bombs 
{
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.BasicObject;
	import com.game.objects.Kind;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BlackBombBox extends BasicObject
	{
		
		public function BlackBombBox() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_WOOD_PLANK;
			myTag = Kind.TAG_BLACK_BOMB_BOX;
			
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new BlackBombBox_mc();	
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _bombBox:BlackBombBox = Universe.getInstance().getFromCache(Kind.TAG_BLACK_BOMB_BOX) as BlackBombBox;
			if (_bombBox == null) _bombBox = new BlackBombBox();
			_bombBox.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{			
			var bodyDef:b2BodyDef = new b2BodyDef();
			var leftWallDef:b2PolygonDef = new b2PolygonDef();
			var rightWallDef:b2PolygonDef = new b2PolygonDef();
			var bottomWallDef:b2PolygonDef = new b2PolygonDef();
			var hitBombPolyDef:b2PolygonDef = new b2PolygonDef();
			
			leftWallDef.density = 50;
			leftWallDef.friction = 0.4;
			leftWallDef.restitution = 0.1;
			leftWallDef.SetAsOrientedBox(12 / 2 / RATIO, 35 / 2 / RATIO, new b2Vec2(-(30 / 2 + 11 / 2) / RATIO, 4 / RATIO), 0);			
			leftWallDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			rightWallDef.density = 50;
			rightWallDef.friction = 0.4;
			rightWallDef.restitution = 0.1; 
			rightWallDef.SetAsOrientedBox(12 / 2 / RATIO, 35 / 2 / RATIO, new b2Vec2((30 / 2 + 11 / 2) / RATIO, 4 / RATIO), 0);
			rightWallDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			bottomWallDef.density = 50;
			bottomWallDef.friction = 0.4;
			bottomWallDef.restitution = 0.1;
			bottomWallDef.SetAsOrientedBox(30 / 2 / RATIO, 12 / 2 / RATIO, new b2Vec2(0, (32 / 2) / RATIO), 0);
			bottomWallDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			hitBombPolyDef.isSensor = true;
			hitBombPolyDef.userData = "HitBlckBobmSensor";
			hitBombPolyDef.density = 50;
			hitBombPolyDef.friction = 0.8;
			hitBombPolyDef.restitution = 0.1;
			hitBombPolyDef.SetAsOrientedBox(25 / 2 / RATIO, 10 / 2 / RATIO, new b2Vec2(0, 10 / RATIO), 0);
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);	
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(leftWallDef);
			_myBody.CreateShape(rightWallDef);
			_myBody.CreateShape(hitBombPolyDef);
			_myBody.CreateShape(bottomWallDef);
			_myBody.SetMassFromShapes();
			
			return _myBody;
		}	
		
	}

}