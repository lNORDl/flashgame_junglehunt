package com.game.objects.bombs 
{
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	import com.framework.math.Amath;
	import com.game.managers.LevelManager;
	import com.game.objects.BasicObject;
	import com.game.objects.Kind;
	import flash.display.MovieClip;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicBomb extends BasicObject
	{		
		public var boomTime:int = 670;
		public var currentTime:int = 0;
		
		public var myBox:b2Body = null;
		public var canBoom:Boolean = true;
		
		public function BasicBomb() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myBox = null;
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{	
			if (type == "COMPLETE")
			{
				stick();
			}
			
			if (type == "BOOM")
			{
				boom();
			}
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		protected function boom():void
		{
			_levelManager.currentLevel.boomBomb();
			
			
			destroy();
		}
		
		private function stick():void
		{
			var jointDef:b2RevoluteJointDef = new b2RevoluteJointDef();					
			jointDef.Initialize(myBox, myBody, myBody.GetWorldCenter());	
			var joint:b2RevoluteJoint = _universe.physics.CreateJoint(jointDef) as b2RevoluteJoint;	
			
			type = "";
			canBoom = false;
			_soundManager.playSound(Kind.SOUND_ADDFLOWER);
			_levelManager.currentLevel.completeBomb();
		}
		
	}

}