package com.game.objects.bombs 
{
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.framework.math.Avector;
	import com.game.objects.Kind;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BlackBomb extends BasicBomb
	{
		
		public function BlackBomb() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_BOMB;
			myTag = Kind.TAG_BLACK_BOMB;
			
			myBody = createMyBody(posX, posY, rot);
			
			var _sp:Avector = new Avector();
			_sp.asSpeed(Amath.random(1, 2), Amath.toRadians(Amath.random(0, 360)));
			myBody.ApplyImpulse(new b2Vec2(_sp.x, _sp.y), myBody.GetWorldCenter());
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new BlackBomb_mc();	
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _bomb:BlackBomb = Universe.getInstance().getFromCache(Kind.TAG_BLACK_BOMB) as BlackBomb;
			if (_bomb == null) _bomb = new BlackBomb();
			_bomb.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{			
			var bodyDef:b2BodyDef = new b2BodyDef();
			var circleDef:b2CircleDef = new b2CircleDef();
			
			circleDef.density = 10;
			circleDef.friction = 0.8;
			circleDef.restitution = 0.1; 
			circleDef.radius = 25 / 2 / RATIO;
			circleDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);	
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(circleDef);
			_myBody.SetMassFromShapes();
			
			return _myBody;
		}		
	}

}