package com.game.objects.walls 
{
	import com.game.objects.BasicObject;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicWall extends BasicObject
	{
		public var arrows:Array = [];
		
		public function BasicWall() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			arrows = [];
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			if (type == "DESTROY")
			{
				destroy();
			}
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			for (var i:int = 0; i < arrows.length; i++)
			{
				var _object:BasicObject = arrows[i];
				_object.destroy();
			}
			
			super.destroy();
		}
		
	}

}