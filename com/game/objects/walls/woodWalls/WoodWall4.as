package com.game.objects.walls.woodWalls 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.objects.walls.BasicWall;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WoodWall4 extends BasicWall
	{
		
		public function WoodWall4() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_WOOD_WALL;
			myTag = Kind.TAG_WOOD_WALL4;
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMyActor():void
		{
			_myActorName = "WoodWall4_mc";
			
			
			super.createMyActor();
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _wall:WoodWall4 = Universe.getInstance().getFromCache(Kind.TAG_WOOD_WALL4) as WoodWall4;
			if (_wall == null) _wall = new WoodWall4();
			_wall.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.density = 0;
			polyDef.friction = Kind.MATERIAL_WOOD_FRICTION;
			polyDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			polyDef.SetAsBox(100 / 2 / RATIO, 15 / 2 / RATIO);
			polyDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}