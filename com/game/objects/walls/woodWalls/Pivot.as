package com.game.objects.walls.woodWalls 
{
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.objects.walls.BasicWall;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class Pivot extends BasicWall
	{
		
		public function Pivot() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_WOOD_WALL;
			myTag = Kind.TAG_PIVOT;
			
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new Pivot_mc();		
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _plank:Pivot = Universe.getInstance().getFromCache(Kind.TAG_PIVOT) as Pivot;
			if (_plank == null) _plank = new Pivot();
			_plank.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var circDef:b2CircleDef = new b2CircleDef();
			
			circDef.density = 0;
			circDef.friction = Kind.MATERIAL_WOOD_FRICTION;
			circDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			circDef.radius = 12 / 2 / RATIO;
			circDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(circDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}