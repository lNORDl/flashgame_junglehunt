package com.game.objects.walls.woodPlanks 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.objects.walls.BasicWall;
	import com.game.Universe;
	import com.game.objects.BasicObject;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WoodBox1 extends BasicWall
	{
		
		public function WoodBox1() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_WOOD_PLANK;
			myTag = Kind.TAG_WOOD_BOX1;		
			
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new WoodBox1_mc();	
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _wall:WoodBox1 = Universe.getInstance().getFromCache(Kind.TAG_WOOD_BOX1) as WoodBox1;
			if (_wall == null) _wall = new WoodBox1();
			_wall.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.density = 10;
			polyDef.friction = 0.6;
			polyDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			
			polyDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			polyDef.SetAsBox(25 / 2 / RATIO, 25 / 2 / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}