package com.game.objects.walls.woodPlanks 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.objects.walls.BasicWall;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WoodPlank3 extends BasicWall
	{
		
		public function WoodPlank3() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_WOOD_PLANK;
			myTag = Kind.TAG_WOOD_PLANK3;
			
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new WoodPlank3_mc();		
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _plank:WoodPlank3 = Universe.getInstance().getFromCache(Kind.TAG_WOOD_PLANK3) as WoodPlank3;
			if (_plank == null) _plank = new WoodPlank3();
			_plank.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.density = 10;
			polyDef.friction = Kind.MATERIAL_WOOD_FRICTION;
			polyDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			polyDef.SetAsBox(80 / 2 / RATIO, 10 / 2 / RATIO);
			polyDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}