package com.game.objects.walls.ironWalls 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.objects.walls.BasicWall;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class IronWall3 extends BasicWall
	{
		
		public function IronWall3() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_IRON_WALL;
			myTag = Kind.TAG_IRON_WALL3;
			mySprite = new IronWall3_mc();			
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _wall:IronWall3 = Universe.getInstance().getFromCache(Kind.TAG_IRON_WALL3) as IronWall3;
			if(_wall == null) _wall = new IronWall3();
			_wall.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.density = 0;
			polyDef.friction = Kind.MATERIAL_IRON_FRICTION;
			polyDef.restitution = Kind.MATERIAL_IRON_RESTITUTION;
			polyDef.SetAsBox(70 / 2 / RATIO, 15 / 2 / RATIO);			
			polyDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE | Kind.CATEGORY_IRON_ROPE | Kind.CATEGORY_WOOD_ARROW | Kind.CATEGORY_IRON_ARROW;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}