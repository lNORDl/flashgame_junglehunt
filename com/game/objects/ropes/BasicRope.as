package com.game.objects.ropes 
{
	import Box2D.Collision.Shapes.b2FilterData;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Dynamics.Joints.b2Joint;
	import com.game.objects.BasicObject;
	import com.game.objects.Kind;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicRope extends BasicObject
	{
		public var myJoints:Array = [];
		private var _isCut:Boolean = false;
		
		public function BasicRope() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			_isCut = false;
			myJoints = [];
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			if (type == "CUT" && !_isCut)
			{
				cutRope();
			}
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			myJoints = [];
			
			super.destroy();
		}
		
		public function cutRope():void
		{
			var j:int;
			var _cn:Boolean = false;
			
			for (j = 0; j < myJoints.length; j++)
			{
				if (j >= 0 && myJoints[j].length > 0)
				{
					_cn = true;
					break;
				}
			}
			
			if (_cn)
			{				
				_soundManager.playSound(Kind.SOUND_CUTROPE);
				
				for (var i:int = 0; i < myJoints[j].length; i++)
				{
					var _joint:b2Joint = myJoints[j][i];
					var _object1:BasicObject = _joint.GetBody1().GetUserData();
					var _object2:BasicObject = _joint.GetBody2().GetUserData();
					
					if (_object1 !== this)
					{
						if (_object1 is BasicRope)
						{
							(_object1 as BasicRope).removeJoint(_joint);
						}
					}
					else if(_object2 !== this)
					{
						if (_object2 is BasicRope)
						{
							(_object2 as BasicRope).removeJoint(_joint);
						}						
					}
					
					_universe.physics.DestroyJoint(_joint);
				}
			}	
			
			_isCut = true;			
		}
		
		public function removeJoint(_joint:b2Joint):void
		{	
			for (var j:int = 0; j < myJoints.length; j++)
			{
				for (var i:int = 0; i < myJoints[j].length; i++)	
				{
					var _myJoint:b2Joint = myJoints[j][i];
					if (_myJoint == _joint)
					{
						myJoints[j][i] = null;
						myJoints[j].splice(i, 1);
						break;
					}
				}
			}
		}
		
	}

}