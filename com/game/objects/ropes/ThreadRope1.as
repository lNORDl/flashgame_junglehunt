package com.game.objects.ropes 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.BasicObject;
	import com.game.objects.Kind;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class ThreadRope1 extends BasicRope
	{
		
		public function ThreadRope1() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_THREAD_ROPE;
			myTag = Kind.TAG_THREAD_ROPE1;	
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMyActor():void
		{
			_myActorName = "ThreadRope1_mc";
			
			
			super.createMyActor();
		}		
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _rope:ThreadRope1 = Universe.getInstance().getFromCache(Kind.TAG_THREAD_ROPE1) as ThreadRope1;
			if(_rope == null) _rope = new ThreadRope1();
			_rope.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.density = 10;
			polyDef.friction = 0.8;
			polyDef.restitution = 0.1;
			polyDef.filter.maskBits = Kind.CATEGORY_THREAD_ROPE;
			
			bodyDef.isBullet = true;
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			polyDef.SetAsBox(4 / 2 / RATIO, 10 / 2 / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}