package com.game.objects 
{
	/**
	 * ...
	 * @author .D.A.N.I.I.L.
	 */
	public class Kind extends Object
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		// Группы
		public static const GROUP_NONE:uint = 0;		
		public static const GROUP_WOOD_WALL:uint = 1;
		public static const GROUP_WOOD_PLANK:uint = 2;		
		public static const GROUP_IRON_WALL:uint = 3;
		public static const GROUP_IRON_PLANK:uint = 4;
		public static const GROUP_ARROW:uint = 5;
		public static const GROUP_DART:uint = 6;
		public static const GROUP_THREAD_ROPE:uint = 7;
		public static const GROUP_IRON_ROPE:uint = 8;
		public static const GROUP_BOMB:uint = 9;
		public static const GROUP_OTHER:uint = 10;
		public static const GROUP_TRIGGER:uint = 11;
		public static const GROUP_BOMB_BOX:uint = 12;
		public static const GROUP_HERO:uint = 13;
		public static const GROUP_PITCHER:uint = 14;
		public static const GROUP_EFFECT:uint = 15;
		public static const GROUP_BLADE:uint = 16;
		
//-------------------------------TAG------------------------------------//
		public static const TAG_NONE:uint = 0;
		
		// Деревянные Стены		
		public static const TAG_WOOD_WALL1:uint = 1;
		public static const TAG_WOOD_WALL2:uint = 2;
		public static const TAG_WOOD_WALL3:uint = 3;
		public static const TAG_WOOD_WALL4:uint = 4;
		public static const TAG_WOOD_WALL5:uint = 5;
		
		// Железные стены
		public static const TAG_IRON_WALL1:uint = 6;
		public static const TAG_IRON_WALL2:uint = 7;
		public static const TAG_IRON_WALL3:uint = 8;
		public static const TAG_IRON_WALL4:uint = 9;
		public static const TAG_IRON_WALL5:uint = 10;
		
		// Палки
		public static const TAG_WOOD_PLANK1:uint = 11;
		public static const TAG_WOOD_PLANK2:uint = 12;
		public static const TAG_WOOD_PLANK3:uint = 13;
		
		// Стрелы
		public static const TAG_WOOD_DART:uint = 14;
		public static const TAG_IRON_DART:uint = 15;
		public static const TAG_BOMB_DART:uint = 16;
		public static const TAG_BOMB_DARTDESTROY:uint = 41;
		
		// Веревки
		public static const TAG_THREAD_ROPE1:uint = 17;
		public static const TAG_THREAD_ROPE2:uint = 18;
		public static const TAG_IRON_ROPE1:uint = 19;
		
		// Бомбы
		public static const TAG_BLACK_BOMB:uint = 20;
		public static const TAG_FRAGILE_BOMB:uint = 21;
		
		// Ящики для бомб
		public static const TAG_BLACK_BOMB_BOX:uint = 22;
		public static const TAG_FRAGILE_BOMB_BOX:uint = 23;
		
		// Триггеры
		public static const TAG_TRIGGER_SHOOT_WOOD_DART:int = 24;
		public static const TAG_TRIGGER_SHOOT_IRON_DART:int = 25;
		public static const TAG_TRIGGER_SHOOT_BOMB_DART:int = 26;	
		public static const TAG_TRIGGER_ADD_WOOD_DART:int = 27;
		public static const TAG_TRIGGER_ADD_IRON_DART:int = 28;
		public static const TAG_TRIGGER_ADD_BOMB_DART:int = 29;
		
		public static const TAG_TRIGGERSHOOT:int = 42;
		public static const TAG_TRIGGERADD:int = 43;
		
		// Кувшины
		public static const TAG_PITCHER1:int = 30;
		public static const TAG_PITCHER2:int = 31;
		public static const TAG_PITCHERDESTROY:int = 32;
		
		// Лезвия
		public static const TAG_WOOD_BLADE:int = 38;
		public static const TAG_IRON_BLADE:int = 39;
		public static const TAG_BOMB_BLADE:int = 40;
		
		// Другое
		public static const TAG_TRIPLE_ROPE_JOINT:uint = 33;
		public static const TAG_WOOD_BOX1:uint = 34;
		public static const TAG_HERO:uint = 35;
		public static const TAG_PIVOT:uint = 36;
		public static const TAG_INWISIBLE_WALL:uint = 37;
//---------------------------------------------------------------------//
		
		// Категории
		public static const CATEGORY_WOOD_ARROW:uint = 1;
		public static const CATEGORY_IRON_ARROW:uint = 2;		
		public static const CATEGORY_THREAD_ROPE:uint = 4;
		public static const CATEGORY_IRON_ROPE:uint = 8;
		public static const CATEGORY_NONE:uint = 16;
		
		// Параметры материалов
		public static const MATERIAL_WOOD_PLANK_DENSITY:int = 1;
		public static const MATERIAL_WOOD_FRICTION:Number = 0.8;
		public static const MATERIAL_WOOD_RESTITUTION:Number = 0.1;
		
		public static const MATERIAL_IRON_DENSITY:int = 1;
		public static const MATERIAL_IRON_FRICTION:Number = 0.8;
		public static const MATERIAL_IRON_RESTITUTION:Number = 0.1;		
		
		public static const MATERIAL_THREAD_DENSITY:int = 1;
		public static const MATERIAL_THREAD_FRICTION:Number = 0.8;
		public static const MATERIAL_THREAD_RESTITUTION:Number = 0.1;
		
//---------------------------------------------------------------------//
		
		// Окна
		public static const WINDOW_STARTLEVEL:int = 5;
		public static const WINDOW_ENDLEVEL:int = 6;
		public static const WINDOW_GAMEINTERFACE:int = 7;
		
		public static const SCREEN_MAINMENU:int = 1;
		public static const SCREEN_LEVELMENU:int = 3;
		public static const SCREEN_GAMELEVEL:int = 2;
		public static const SCREEN_ABOUT:int = 4;
		
		// Звуки
		public static const SOUND_SHOOTARROW:int = 1;
		public static const SOUND_PITCHERDESTROY:int = 2;
		public static const SOUND_CUTROPE:int = 3;
		public static const SOUND_ADDARROW:int = 4;
		public static const SOUND_ARROWEXPLOSION:int = 5;
		public static const SOUND_ADDFLOWER:int = 6;
		public static const SOUND_LEVELCOMPLETE:int = 7;
		public static const SOUND_STICKARROW:int = 8;
		public static const SOUND_BUTTONCLICK:int = 9;
	}
}

		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------