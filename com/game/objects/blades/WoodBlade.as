package com.game.objects.blades 
{
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.Universe;
	import com.game.objects.BasicObject;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class WoodBlade extends BasicObject
	{
		
		public function WoodBlade() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_BLADE;
			myTag = Kind.TAG_WOOD_BLADE;		
			myBody = createMyBody(posX, posY, rot);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function createMySprite():void
		{
			mySprite = new WoodBlade_mc();
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _wall:WoodBlade = Universe.getInstance().getFromCache(Kind.TAG_WOOD_BLADE) as WoodBlade;
			if (_wall == null) _wall = new WoodBlade();
			_wall.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{
			
			
			super.update(delta);
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var circDef:b2CircleDef = new b2CircleDef();
			var tipDef:b2CircleDef = new b2CircleDef();
			
			tipDef.isSensor = true;
			tipDef.density = 1;
			tipDef.friction = 0.6;
			tipDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			tipDef.radius = 20 / 2 / RATIO;
			tipDef.filter.categoryBits = Kind.CATEGORY_THREAD_ROPE;
			
			circDef.density = 7;
			circDef.friction = 0.6;
			circDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			circDef.radius = 20 / 2 / RATIO;
			circDef.filter.categoryBits = Kind.CATEGORY_IRON_ROPE;
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(circDef);
			_myBody.CreateShape(tipDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}