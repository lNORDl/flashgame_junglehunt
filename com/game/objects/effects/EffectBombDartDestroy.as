package com.game.objects.effects 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.Universe;
	/**
	 * ...
	 * @author ...
	 */
	public class EffectBombDartDestroy extends BasicEffect
	{
		
		public function EffectBombDartDestroy() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_EFFECT;
			myTag = Kind.TAG_BOMB_DARTDESTROY;	
			myBody = createMyBody(posX, posY, rot);
			_animLength = 17;
			
			
			super.init(posX, posY, rot);
			
			myActor.gotoAndPlay(1);
		}
		
		override protected function createMyActor():void
		{
			_myActorName = "BombDartDestroy_eff";
			
			super.createMyActor();
			
			myActor.width = 70;
			myActor.height = 70;
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _effect:EffectBombDartDestroy = Universe.getInstance().getFromCache(Kind.TAG_BOMB_DARTDESTROY) as EffectBombDartDestroy;
			if (_effect == null) _effect = new EffectBombDartDestroy();
			_effect.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.isSensor = true;
			polyDef.density = 0;
			polyDef.friction = Kind.MATERIAL_WOOD_FRICTION;
			polyDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			polyDef.SetAsBox(10 / 2 / RATIO, 10 / 2 / RATIO);
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}