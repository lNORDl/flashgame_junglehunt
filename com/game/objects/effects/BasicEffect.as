package com.game.objects.effects 
{
	import com.game.objects.BasicObject;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicEffect extends BasicObject
	{
		
		protected var _animLength:int;
		
		public function BasicEffect() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update(delta:Number):void
		{	
			_animLength --;
			if (_animLength == 3)
			{
				myActor.stop();
				destroy();
			}
			else super.update(delta);
		}
		
	}

}