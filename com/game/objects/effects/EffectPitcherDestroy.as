package com.game.objects.effects 
{
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import com.framework.math.Amath;
	import com.game.objects.Kind;
	import com.game.Universe;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class EffectPitcherDestroy extends BasicEffect
	{
		
		public function EffectPitcherDestroy() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			myGroup = Kind.GROUP_EFFECT;
			myTag = Kind.TAG_PITCHERDESTROY;	
			myBody = createMyBody(posX, posY, rot);
			_animLength = 13;
			
			
			super.init(posX, posY, rot);
			
			myActor.gotoAndPlay(1);
		}
		
		override protected function createMyActor():void
		{
			_myActorName = "PitcherDestroy_eff";
			
			super.createMyActor();
			
			myActor.width = 50;
			myActor.height = 50;
		}
		
		public static function create(posX:Number, posY:Number, rot:Number):void
		{
			var _effect:EffectPitcherDestroy = Universe.getInstance().getFromCache(Kind.TAG_PITCHERDESTROY) as EffectPitcherDestroy;
			if (_effect == null) _effect = new EffectPitcherDestroy();
			_effect.init(posX, posY, rot);
		}
		
		private function createMyBody(posX:Number, posY:Number, rot:Number):b2Body 
		{
			var bodyDef:b2BodyDef = new b2BodyDef();
			var polyDef:b2PolygonDef = new b2PolygonDef();
			
			polyDef.isSensor = true;
			polyDef.density = 0;
			polyDef.friction = Kind.MATERIAL_WOOD_FRICTION;
			polyDef.restitution = Kind.MATERIAL_WOOD_RESTITUTION;
			polyDef.SetAsBox(10 / 2 / RATIO, 10 / 2 / RATIO);
			
			bodyDef.userData = this;
			bodyDef.angle = Amath.toRadians(rot);
			bodyDef.position.Set(posX / RATIO, posY / RATIO);
			
			var _myBody:b2Body = _universe.physics.CreateBody(bodyDef);
			_myBody.CreateShape(polyDef);
			_myBody.SetMassFromShapes();	
			
			return _myBody;
		}
		
	}

}