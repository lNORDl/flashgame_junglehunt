package com.game.controllers
{	
	import com.game.interfaces.IGameObject;
	import com.game.objects.BasicObject;
	import com.game.Universe;
	
	public class ObjectController extends Object
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var objects:Array = [];
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		private var _universe:Universe = Universe.getInstance();
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
		
		/**
		 * @constructor
		 */
		public function ObjectController()
		{
			// nothing
		}
		
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		/**
		 * Добавляет объект в контроллер.
		 */
		public function add(obj:IGameObject):void
		{
			objects.push(obj);
		}
		
		/**
		 * Удаляет объект из контроллера.
		 */
		public function remove(obj:IGameObject):void
		{
			for (var i:int = 0; i < objects.length; i++)
			{
				if (objects[i] == obj)
				{
					objects[i] = null;
					objects.splice(i, 1);
					break;
				}
			}
		}
		
		/**
		 * Удаляет все объекты.
		 */
		public function clear():void
		{
			//for (var i:int = 0; i < objects.length; i++)
			//{
				//trace(objects.length);
				//var _object:BasicObject = objects[i];
				//_object.destroy();
				//trace(objects.length);
			//}
			while (objects.length > 0)
			{
				objects[0].destroy();
			}
			//objects = [];
		}
		
		/**
		 * Процессит все объекты находящиеся в контроллере.
		 */
		public function update(delta:Number):void
		{
			for (var i:int = 0; i < objects.length; i++)
			{
				var _object:BasicObject = objects[i];
				if (_object !== null && _object.isExists)
				{
					objects[i].update(delta);
				}
			}
		}

	}
}