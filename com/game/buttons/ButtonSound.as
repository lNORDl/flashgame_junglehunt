package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import flash.geom.Point;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class ButtonSound extends BasicButton
	{
		
		public function ButtonSound() 
		{
			
		}
		
		override public function init(screen, position:Point):void
		{			
			_myActor = new AntActor();
			
			if (_soundManager.isPlay)
			{
				_myActor.addAnimFromCache("ButtonSound_STATETOGGLEON_mc", "STATE_ON", true);
				_myActor.addAnimFromCache("ButtonSound_STATETOGGLEOFF_mc", "STATE_OFF", false);
				_isToggleOff = false;
				_isToggleOn = true;
			}	
			
			if (!_soundManager.isPlay)
			{
				_myActor.addAnimFromCache("ButtonSound_STATETOGGLEON_mc", "STATE_ON", false);
				_myActor.addAnimFromCache("ButtonSound_STATETOGGLEOFF_mc", "STATE_OFF", true);
				_isToggleOff = true;
				_isToggleOn = false;
			}
			
			_isToggle = true;
			
			
			super.init(screen, position);
		}
		
		public static function create(screen, position:Point):BasicButton
		{
			var _button:BasicButton = new ButtonSound();
			_button.init(screen, position);
			
			return _button;
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		override protected function cl():void
		{
			super.cl();
			
			if (_isToggleOn)
			{
				_soundManager.paused = true;
			}
			
			if (_isToggleOff)
			{
				_soundManager.paused = false;
			}
		}
		
	}

}