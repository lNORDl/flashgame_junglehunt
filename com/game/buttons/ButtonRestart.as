package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import com.game.levels.BasicLevel;
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import com.game.screens.ScreenGameLevel;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonRestart extends BasicButton
	{
		
		public function ButtonRestart() 
		{
			
		}
		
		override public function init(screen, position:Point):void
		{			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ButtonRestart_STATEOUT_mc", "STATE_OUT", true);
			_myActor.addAnimFromCache("ButtonRestart_STATEOVER_mc", "STATE_OVER", false);
			_myActor.addAnimFromCache("ButtonRestart_STATECLICK_mc", "STATE_CLICK", false);
			
			
			super.init(screen, position);
		}
		
		public static function create(screen, position:Point):BasicButton
		{
			var _button:BasicButton = new ButtonRestart();
			_button.init(screen, position);
			
			return _button;
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		override protected function cl():void
		{
			super.cl();
			
			
			var _gameScreen:ScreenGameLevel = _screenManager.setScreen(Kind.SCREEN_GAMELEVEL) as ScreenGameLevel;
			_gameScreen.numLevel = _levelManager.currentLevel.numLevel;
		}
		
	}

}