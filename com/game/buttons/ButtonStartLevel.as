package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import com.game.managers.LevelManager;
	import com.game.objects.Kind;
	import com.game.screens.ScreenGameLevel;
	import flash.display.MovieClip;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonStartLevel extends BasicButton
	{
		
		public var myLevelNum:int = 0;
		private var _infoPanel:MovieClip;
		private var _isLock:Boolean;
		
		public function ButtonStartLevel() 
		{
			
		}
		
		override public function init(screen, position:Point):void
		{			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ButtonStartLevel_STATEOUT_mc", "STATE_OUT", true);
			_myActor.addAnimFromCache("ButtonStartLevel_STATEOVER_mc", "STATE_OVER", false);
			_myActor.addAnimFromCache("ButtonStartLevel_STATECLICK_mc", "STATE_CLICK", false);
			
			
			super.init(screen, position);
			
			_infoPanel = new InfoPanel_mc();
			addChild(_infoPanel);
			
			_infoPanel.x = 0;
			_infoPanel.y = 4;
			
			_infoPanel.TextNumLevel.text = myLevelNum.toString();
			
			_isLock = false;
			
			if (myLevelNum > _levelManager.unlockLevel)
			{
				_isLock = true;
				_infoPanel.Star.gotoAndStop("LOCK");
			}
			else if (myLevelNum == _levelManager.unlockLevel)
			{
				_infoPanel.Star.gotoAndStop("UNLOCK");
			}
			else 
			{
				_infoPanel.Star.gotoAndStop(_levelManager.completeType[myLevelNum - 1]);
			}
		}
		
		public static function create(screen, position:Point, num:int):BasicButton
		{
			var _button:BasicButton = new ButtonStartLevel();
			(_button as ButtonStartLevel).myLevelNum = num;
			_button.init(screen, position);
			
			
			return _button;
		}
		
		override public function destroy():void
		{
			removeChild(_infoPanel);
			_infoPanel = null;
			
			super.destroy();
		}
		
		override protected function cl():void
		{
			if (!_isLock)
			{
				super.cl();
				
				
				var _gameScreen:ScreenGameLevel = _screenManager.setScreen(Kind.SCREEN_GAMELEVEL) as ScreenGameLevel;
				_gameScreen.numLevel = myLevelNum;
			}
		}
		
		//override protected function mOut():void
		//{
			//if (!_isLock)
			//{	
				//super.mOut();
			//}
		//}
		
		//override protected function mOver():void
		//{
			//if (!_isLock)
			//{
				//super.mOut();
			//}
		//}
		
	}

}