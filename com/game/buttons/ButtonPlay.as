package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import com.game.screens.ScreenGameLevel;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonPlay extends BasicButton
	{
		
		public function ButtonPlay() 
		{
			
		}
		
		override public function init(screen, position:Point):void
		{			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ButtonPlay_STATEOUT_mc", "STATE_OUT", true);
			_myActor.addAnimFromCache("ButtonPlay_STATEOVER_mc", "STATE_OVER", false);
			_myActor.addAnimFromCache("ButtonPlay_STATECLICK_mc", "STATE_CLICK", false);
			
			
			super.init(screen, position);
		}
		
		public static function create(screen, position:Point):BasicButton
		{
			var _button:BasicButton = new ButtonPlay();
			_button.init(screen, position);
			
			return _button;
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		override protected function cl():void
		{
			super.cl();
			
			_screenManager.setScreen(Kind.SCREEN_LEVELMENU);
		}
		
	}

}