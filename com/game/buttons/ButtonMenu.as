package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import com.game.objects.Kind;
	import flash.geom.Point;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonMenu extends BasicButton
	{
		
		public function ButtonMenu() 
		{
			
		}
		
		override public function init(screen, position:Point):void
		{			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ButtonMenu_STATEOUT_mc", "STATE_OUT", true);
			_myActor.addAnimFromCache("ButtonMenu_STATEOVER_mc", "STATE_OVER", false);
			_myActor.addAnimFromCache("ButtonMenu_STATECLICK_mc", "STATE_CLICK", false);
			
			
			super.init(screen, position);
		}
		
		public static function create(screen, position:Point):BasicButton
		{
			var _button:BasicButton = new ButtonMenu();
			_button.init(screen, position);
			
			return _button;
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		override protected function cl():void
		{
			super.cl();
			
			_screenManager.setScreen(Kind.SCREEN_MAINMENU);
		}
		
	}

}