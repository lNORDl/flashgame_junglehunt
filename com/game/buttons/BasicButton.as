package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import com.game.Game;
	import com.game.managers.LevelManager;
	import com.game.managers.ScreenManager;
	import com.game.managers.SoundManager;
	import com.game.objects.Kind;
	import com.game.screens.BasicScreen;
	import com.game.Universe;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	/**
	 * ...
	 * @author .D.N.I.I.L.
	 */
	public class BasicButton extends MovieClip
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		protected var _game:Game = Game.getInstance();
		protected var _screenManager:ScreenManager = ScreenManager.getInstance();
		protected var _levelManager:LevelManager = LevelManager.getInstance();
		protected var _soundManager:SoundManager = SoundManager.getInstance();
		protected var _myScreen;
		
		protected var _myActor:AntActor;
		protected var _myPosition:Point;
		
		protected var _isToggle:Boolean = false;
		protected var _isToggleOn:Boolean = true;
		protected var _isToggleOff:Boolean = false;
		
		//---------------------------------------
		// PRIVATE VARIABLES
		//---------------------------------------
		
		
		public function BasicButton() 
		{
			
		}
		
		public function init(screen, position:Point):void
		{
			_myScreen = screen;
			_myPosition = position;			
			
			if (_myActor !== null)
			{
				addChild(_myActor);
				_myActor.buttonMode = true;
			}
			
			if (_myPosition !== null)
			{
				this.x = _myPosition.x;
				this.y = _myPosition.y;				
			}
			
			_myScreen.addChild(this);
			_myScreen.addButton(this);
			
			
			this.addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, mouseOut);
			this.addEventListener(MouseEvent.CLICK, mouseClick);
		}
		
		public function destroy():void
		{			
			this.removeEventListener(MouseEvent.MOUSE_OVER, mouseOver);
			this.removeEventListener(MouseEvent.MOUSE_OUT, mouseOut);
			this.removeEventListener(MouseEvent.CLICK, mouseClick);
			
			_myScreen.removeChild(this);
			_myActor.free();
			_myScreen = null;
			_myPosition = null;			
			_myActor = null;
			_game = null;
			_levelManager = null;
			_screenManager = null;
			_soundManager = null;
		}
		
		private function mouseClick(e:MouseEvent):void 
		{			
			if (_myScreen.canUse) cl();
		}
		
		private function mouseOut(e:MouseEvent):void 
		{
			if (!_isToggle)
			{
				_myActor.switchAnim("STATE_OUT");
				if (_myScreen.canUse) mOut();
			}
		}
		
		private function mouseOver(e:MouseEvent):void 
		{
			if (!_isToggle)
			{
				_myActor.switchAnim("STATE_OVER");
				if (_myScreen.canUse) mOver();
			}
		}
		
		protected function mOut():void 
		{
			
		}
		
		protected function mOver():void 
		{
			
		}
		
		protected function cl():void 
		{
			_soundManager.playSound(Kind.SOUND_BUTTONCLICK);
			
			if (!_isToggle)
			{
				_myActor.switchAnim("STATE_CLICK");
			}
			else
			{
				if (_isToggleOn)
				{
					_isToggleOn = false;
					_isToggleOff = true;
					_myActor.switchAnim("STATE_OFF");
				}
				else if (_isToggleOff)
				{
					_isToggleOn = true;
					_isToggleOff = false;
					_myActor.switchAnim("STATE_ON");					
				}
			}
		}
		
	}

}