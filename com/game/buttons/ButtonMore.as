package com.game.buttons 
{
	import com.antkarlov.animation.AntActor;
	import com.game.objects.Kind;
	import flash.geom.Point;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class ButtonMore extends BasicButton
	{
		
		public function ButtonMore() 
		{
			
		}
		
		override public function init(screen, position:Point):void
		{			
			_myActor = new AntActor();
			_myActor.addAnimFromCache("ButtonMore_STATEOUT_mc", "STATE_OUT", true);
			_myActor.addAnimFromCache("ButtonMore_STATEOVER_mc", "STATE_OVER", false);
			_myActor.addAnimFromCache("ButtonMore_STATECLICK_mc", "STATE_CLICK", false);
			
			
			super.init(screen, position);
		}
		
		public static function create(screen, position:Point):BasicButton
		{
			var _button:BasicButton = new ButtonMore();
			_button.init(screen, position);
			
			return _button;
		}
		
		override public function destroy():void
		{
			
			
			super.destroy();
		}
		
		override protected function cl():void
		{
			
		}
		
	}

}